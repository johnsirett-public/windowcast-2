# WindowCast 2 for Libretro

Libretro core to capture the contents of another window for video processing. This is useful, for say, capturing the output of a standalone emulator (like xemu, Dolphin standalone, RPCS3 or PCSX2 nightlies) or a PC game running in a window and then processing it with RetroArch's shader stack.

This core strictly requires the 'vulkan' RetroArch video driver, for performance reasons. Currently, Windows and Linux platforms are supported via different capture methods, listed below:

- WGC (Windows Graphics Capture) for Windows 10, version 1803 or later.
- XComposite for Linux/X11. Supports most WMs/DEs.
- Pipewire for Linux. This is supported by the Gamescope Wayland compositor for capture in the Gaming Mode of the Steam Deck, amongst other situations.

Audio or input is not handled, and it is expected that the game will be running in the background.

## System Requirements

- Fast modern GPU which supports Vulkan 1.1.
- On Linux/X11:
    - X Server and display driver with support for the X Composite Extension 0.2+, and Extended Window Manager Hints (EWMH).
- On Windows:
    - Windows 10, version 1803 or later.
    - Your GPU must also support Direct3D 10/11.
    - Support for the VK_KHR_external_memory_win32 extension. Make sure your GPU drivers are up-to-date.

## General Usage

Install core and info file into RetroArch's 'cores' and 'info' directories. You may need to set the RetroArch option 'core_info_cache_enable = "false"' for the core description to be properly detected.

Start RetroArch and 'Load Content', then load the 'partials-example.txt' file or another text file describing what windows the core will attempt to find and capture.

If the wrong window is detected at any point, use the 'Restart' option from RetroArch's quick menu. Options related to scaling, aspect ratio correction and cropping of the captured window can be found in the Core Options menu, and are documented there.

## Capture Partials Files

WindowsCast loads .txt files as it's content, which are known as 'capture partial' files. These hint WindowCast as to what window to select for capture.

TL;DR Load a .txt with one line, where this line is usually some part of the title of the window you want to capture.

---

Each line in the file lists a different partial. Comment using '#' are supported. Each partial is tested independently, and the window was produces the best match to any partial in the file will be selected. In this way, a large list of different partials can be used to say, hint the core to select known video games or standalone emulators rather than other software.

You can also weight individual partials by using the syntax `capture partial [0.12345]`, if you want to core to prefer one partial over another when multiple partials are listed in the file.

Various properties of individual windows will be matched against the provided partials. These differ on a per platform basis, but generally include:

- The window's title; the text displayed in the titlebar.
- Key internal window text, such as the 'classname' on Windows.
- The name of the program which owns the window.

## Tips and Tricks

- The game or emulator should be running in a window, and emulators should be set to accept background input if possible. For visual results you should set the game resolution to a low resolution like 640x480 or 800x600, or use 1x native settings in your emulator. On Windows, [Sizer](http://brianapps.net/sizer4/) can help with resizing windows to an exact resolution if you can't set the window resolution in the game/emulator settings.
- On Windows, some users with Nvidia GPU may experience issues with the capture image in RetroArch 'freezing' in fullscreen mode when certain applications are used. To help resolve this, open the Nvidia Control Panel. In the "Manage 3D Settings" tab, change the option "Vulkan/OpenGL present method" to "Prefer layered on DXGI Swapchain". If this option does not appear, you need to update your GPU drivers.
- Windows 11 users should use [Win11DisableRoundedCorners](https://github.com/valinet/Win11DisableRoundedCorners) to fix a visual bug that arises due to new DWM (Desktop Window Manager) behaviour.