#include "PipewireCaptureBackend.h"

#include <fcntl.h>
#include <regex>
#include <spa/debug/format.h>
#include <spa/utils/result.h>
#include "VulkanFrontend.h"

using std::make_shared;

extern retro_log_printf_t                   logCb;
extern retro_hw_render_interface_vulkan*    hwInterface;

std::string PipewireCaptureBackend::GetObjectPathSafeSenderName()
{
    std::string senderName = (g_dbus_connection_get_unique_name(this->dbusConnection) + 1);
    senderName = std::regex_replace(senderName, std::regex("\\."), "_");
    return senderName;
}

void PipewireCaptureBackend::CreateRequestPath(std::string* outPath, std::string* outToken)
{
    static uint32_t requestTokenCount = 0;
    requestTokenCount++;
    if (outToken) {
        *outToken = "wcast"; *outToken += std::to_string(requestTokenCount);
    }

    if (outPath) {
        std::string senderName = GetObjectPathSafeSenderName();
        *outPath = "/org/freedesktop/portal/desktop/request/";
        *outPath += senderName; *outPath += "/"; *outPath += *outToken;
    }
}

void PipewireCaptureBackend::CreateSessionPath(std::string* outPath, std::string* outToken)
{
    static uint32_t sessionTokenCount = 0;
    sessionTokenCount++;
    if (outToken) {
        *outToken = "wcast"; *outToken += std::to_string(sessionTokenCount);
    }

    if (outPath) {
        std::string senderName = GetObjectPathSafeSenderName();
        *outPath = "/org/freedesktop/portal/desktop/session/";
        *outPath += senderName; *outPath += "/"; *outPath += *outToken;
    }
}

void XDPSignalSubscription::OnCancelled(GCancellable* cancellable, void* data)
{
    XDPSignalSubscription* signalSub = (XDPSignalSubscription*)data;

    g_dbus_connection_call(signalSub->dbusConnection, "org.freedesktop.portal.Desktop",
        signalSub->requestPath.c_str(), "org.freedesktop.portal.Request", "Close",
        nullptr, nullptr, G_DBUS_CALL_FLAGS_NONE, -1, nullptr, nullptr, nullptr);

    delete signalSub;
}

void XDPSignalSubscription::OnResponseReceived(GDBusConnection* connection,
        const char* senderName, const char* objectPath, const char* interfaceName,
        const char* signalName, GVariant* parameters, void* userData)
{
    XDPSignalSubscription* signalSub = (XDPSignalSubscription*)userData;

    if (signalSub->callback)
        signalSub->callback(parameters, signalSub->userData);
}

XDPSignalSubscription::XDPSignalSubscription(GDBusConnection* dbusConnection, const char* requestPath,
    GCancellable* cancellable, XDPSignalRealCallback callback, gpointer userData)
{
    // Store local signal information.
    this->dbusConnection = dbusConnection;
    this->requestPath = requestPath;
    this->callback = callback;
    this->userData = userData;
    this->cancellable = cancellable ? g_object_ref(cancellable) : nullptr;
    this->cancelledId = cancellable ? g_signal_connect(cancellable, "cancelled",
        G_CALLBACK(XDPSignalSubscription::OnCancelled), this) : 0;

    // Acutally subscribe to the signal.
    this->signalId = g_dbus_connection_signal_subscribe(this->dbusConnection,
        "org.freedesktop.portal.Desktop", "org.freedesktop.portal.Request", "Response",
        this->requestPath.c_str(), nullptr, G_DBUS_SIGNAL_FLAGS_NO_MATCH_RULE,
        XDPSignalSubscription::OnResponseReceived, this, nullptr);
}

XDPSignalSubscription::~XDPSignalSubscription()
{
    if (this->signalId)
        g_dbus_connection_signal_unsubscribe(this->dbusConnection, this->signalId);
    if (this->cancelledId > 0)
        g_signal_handler_disconnect(this->cancellable, this->cancelledId);
}

bool PipewireCaptureBackend::PipewireConnectFd(int pipewireFd)
{
    // Setup local PipeWire structures.
    this->pipewireFd = pipewireFd;
    this->pipewireThreadLoop = pw_thread_loop_new("PipeWire thread loop", nullptr);
    this->pipewireContext = pw_context_new(pw_thread_loop_get_loop(this->pipewireThreadLoop), nullptr, 0);

    if (pw_thread_loop_start(this->pipewireThreadLoop)) {
        return false;
    }

    pw_thread_loop_lock(this->pipewireThreadLoop);

    this->pipewireCore = pw_context_connect_fd(this->pipewireContext,
        fcntl(this->pipewireFd, F_DUPFD_CLOEXEC, 5), nullptr, 0);
    if(!this->pipewireCore) {
        logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't create PipeWire core.\n");
        pw_thread_loop_unlock(this->pipewireThreadLoop);
        return false;
    }

    static const pw_core_events coreEvents = {
        PW_VERSION_CORE_EVENTS,
        .info = PipewireCaptureBackend::OnCoreInfo,
        .done = PipewireCaptureBackend::OnCoreDone,
        .error = PipewireCaptureBackend::OnCoreError
    };
    pw_core_add_listener(this->pipewireCore, &this->pipewireCoreListener, &coreEvents, this);

    // Dispatch a core sync now to ensure we get the 'info' event and our first 'core done'.
    pipewireSyncId = pw_core_sync(this->pipewireCore, PW_ID_CORE, this->pipewireSyncId);
    pw_thread_loop_wait(this->pipewireThreadLoop);

    pw_thread_loop_unlock(this->pipewireThreadLoop);

    return true;
}

void PipewireCaptureBackend::OnCoreInfo(void* userData, const pw_core_info *info)
{
    logCb(RETRO_LOG_INFO, "PipeWire Capture Backend: Connected to PipeWire, server version %s\n", info->version);
}

void PipewireCaptureBackend::OnCoreDone(void* userData, uint32_t id, int seq)
{
    PipewireCaptureBackend* captureBackend = (PipewireCaptureBackend*)userData;

    if(id == PW_ID_CORE && captureBackend->pipewireSyncId == seq)
        pw_thread_loop_signal(captureBackend->pipewireThreadLoop, false);
}

void PipewireCaptureBackend::OnCoreError(void* userData, uint32_t id, int seq, int res, const char *message)
{
    PipewireCaptureBackend* captureBackend = (PipewireCaptureBackend*)userData;

    logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: PipeWire Core Error id:%u seq:%d res:%d (%s): %s\n",
        id, seq, res, spa_strerror(res), message);

    pw_thread_loop_signal(captureBackend->pipewireThreadLoop, false);
}

void PipewireCaptureBackend::PipewireConnectStream()
{
    pw_thread_loop_lock(this->pipewireThreadLoop);

    // Create the new PipeWire stream object.
    pw_properties* streamProperties = pw_properties_new(
        PW_KEY_MEDIA_TYPE, "Video",
        PW_KEY_MEDIA_CATEGORY, "Capture",
        PW_KEY_MEDIA_ROLE, "Screen",
        nullptr);
    this->pipewireStream = pw_stream_new(this->pipewireCore, "WindowCast PipeWire Stream", streamProperties);

    static const pw_stream_events streamEvents = {
        PW_VERSION_STREAM_EVENTS,
        .param_changed = PipewireCaptureBackend::OnParamChanged,
        .process = PipewireCaptureBackend::OnProcess
    };
    pw_stream_add_listener(this->pipewireStream, &this->pipewireStreamListener, &streamEvents, this);

    // Set acceptable format parameters.
    uint8_t paramsBuffer[1024];
    spa_pod_builder podBuilder = SPA_POD_BUILDER_INIT(paramsBuffer, sizeof(paramsBuffer));
    spa_pod_frame formatFrame;
    spa_pod_builder_push_object(&podBuilder, &formatFrame, SPA_TYPE_OBJECT_Format, SPA_PARAM_EnumFormat);
    spa_pod_builder_add(&podBuilder, SPA_FORMAT_mediaType, SPA_POD_Id(SPA_MEDIA_TYPE_video), 0);
    spa_pod_builder_add(&podBuilder, SPA_FORMAT_mediaSubtype, SPA_POD_Id(SPA_MEDIA_SUBTYPE_raw), 0);
    spa_pod_builder_add(&podBuilder, SPA_FORMAT_VIDEO_format, SPA_POD_Id(SPA_VIDEO_FORMAT_BGRA), 0);
    const spa_pod* params = (spa_pod*)spa_pod_builder_pop(&podBuilder, &formatFrame);

    // Connect to the PipeWire node already found.
    pw_stream_connect(this->pipewireStream, PW_DIRECTION_INPUT, this->pipewireNode, 
        (pw_stream_flags)(PW_STREAM_FLAG_AUTOCONNECT | PW_STREAM_FLAG_MAP_BUFFERS), &params, 1);

    pw_thread_loop_unlock(this->pipewireThreadLoop);
}

void PipewireCaptureBackend::OnParamChanged(void* userData, uint32_t id, const spa_pod* param)
{
    PipewireCaptureBackend* captureBackend = (PipewireCaptureBackend*)userData;

    // Only need to respond to certain parameter changes.
    if (!param || id != SPA_PARAM_Format)
        return;
    
    int result = spa_format_parse(param,
        &captureBackend->pipewireStreamFormat.media_type,
        &captureBackend->pipewireStreamFormat.media_subtype);
    if (result < 0)
        return;
    
    if (captureBackend->pipewireStreamFormat.media_type != SPA_MEDIA_TYPE_video ||
        captureBackend->pipewireStreamFormat.media_subtype != SPA_MEDIA_SUBTYPE_raw)
        return;

    // Parse out specific raw format data.
    spa_format_video_raw_parse(param, &captureBackend->pipewireStreamFormat.info.raw);

    // Log basic stream info.
    const char* formatName = spa_debug_type_find_name(spa_type_video_format,
        captureBackend->pipewireStreamFormat.info.raw.format);
    logCb(RETRO_LOG_INFO, "PipeWire Capture Backend: \tStream format %d (%s)\n",
        captureBackend->pipewireStreamFormat.info.raw.format, formatName ? formatName : "unknown");
    logCb(RETRO_LOG_INFO, "PipeWire Capture Backend: \tStream size %dx%d\n",
        captureBackend->pipewireStreamFormat.info.raw.size.width,
        captureBackend->pipewireStreamFormat.info.raw.size.height);
    logCb(RETRO_LOG_INFO, "PipeWire Capture Backend: \tStream framerate (0/1 implies variable framerate) %d/%d\n",
        captureBackend->pipewireStreamFormat.info.raw.framerate.num,
        captureBackend->pipewireStreamFormat.info.raw.framerate.denom);

    // Check if Vulkan image strucutres need to be recreated.
    if(!captureBackend->pipewireStreamNegotiated ||
        captureBackend->imageExtent.width != captureBackend->pipewireStreamFormat.info.raw.size.width ||
        captureBackend->imageExtent.height != captureBackend->pipewireStreamFormat.info.raw.size.height)
        captureBackend->CreateNewVulkanStructures();

    captureBackend->pipewireStreamNegotiated = true;
}

void PipewireCaptureBackend::OnProcess(void* userData)
{
    PipewireCaptureBackend* captureBackend = (PipewireCaptureBackend*)userData;

    // Find the lastest buffer from PipeWire.
    pw_buffer* pipewireBuffer = nullptr;
    while (true) {
        pw_buffer* auxBuffer = pw_stream_dequeue_buffer(captureBackend->pipewireStream);
        if (!auxBuffer)
            break;
        if (pipewireBuffer)
            pw_stream_queue_buffer(captureBackend->pipewireStream, pipewireBuffer);
        pipewireBuffer = auxBuffer;
    }
    if(!pipewireBuffer) {
        logCb(RETRO_LOG_WARN, "PipeWire Capture Backend: No more buffers!\n");
        return;
    }

    // TODO: limited implementation for now, plently of cases not handled.

    spa_buffer* buffer = pipewireBuffer->buffer;
    spa_meta_header* header = (spa_meta_header*)spa_buffer_find_meta_data(buffer, SPA_META_Header, sizeof(*header));
    if(header && (header->flags & SPA_META_HEADER_FLAG_CORRUPTED) > 0) {
        logCb(RETRO_LOG_WARN, "PipeWire Capture Backend: Corrupt buffer.\n");
        pw_stream_queue_buffer(captureBackend->pipewireStream, pipewireBuffer);
        return;
    }
    if((buffer->datas[0].chunk->flags & SPA_CHUNK_FLAG_CORRUPTED) > 0) {
        logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Buffer contains corrupted data.\n");
        pw_stream_queue_buffer(captureBackend->pipewireStream, pipewireBuffer);
        return;
    }
    if(buffer->datas[0].chunk->size == 0) {
        logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Buffer contained no data.\n");
        pw_stream_queue_buffer(captureBackend->pipewireStream, pipewireBuffer);
        return;
    }

    // Map the staging buffer -- we will IMMEDIATELY load it with new data.
    captureBackend->stagingBufferLock = true;
    VkDeviceSize copySize = captureBackend->pipewireStreamFormat.info.raw.size.width *
        captureBackend->pipewireStreamFormat.info.raw.size.height * 4;
    void* data;
    vkMapMemory(hwInterface->device, captureBackend->stagingBufferMemory, 0, copySize, 0, &data);
    memcpy(data, buffer->datas[0].data, static_cast<size_t>(copySize));
    vkUnmapMemory(hwInterface->device, captureBackend->stagingBufferMemory);
    captureBackend->stagingBufferLock = false;

    pw_stream_queue_buffer(captureBackend->pipewireStream, pipewireBuffer);
}

void PipewireCaptureBackend::OnPipewireRemoteOpened(GObject* source, GAsyncResult* inResult, void* userData)
{
    PipewireCaptureBackend* captureBackend = (PipewireCaptureBackend*)userData;

    GUnixFDList* fdList = nullptr;
    GVariant* result = nullptr;
    GError* error = nullptr;
    result = g_dbus_proxy_call_with_unix_fd_list_finish(G_DBUS_PROXY(source), &fdList, inResult, &error);
    if (error) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't retrieve pipewire fd: %s\n", error->message);
        g_free(fdList);
        g_variant_unref(result);
        g_error_free(error);
		return;
	}

    int fdIndex;
    g_variant_get(result, "(h)", &fdIndex, &error);

    if (error) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't retrieve pipewire fd: %s\n", error->message);
        g_free(fdList);
        g_variant_unref(result);
        g_error_free(error);
		return;
	}

    // Finally... we can get the real PipeWire Fd and connect to Pipewire.
    int pipewireFd = g_unix_fd_list_get(fdList, fdIndex, &error);
    if (error) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't retrieve pipewire fd: %s\n", error->message);
        g_free(fdList);
        g_variant_unref(result);
        g_error_free(error);
		return;
	}

    if(!captureBackend->PipewireConnectFd(pipewireFd))
        return;

    // And connect to the stream.
    captureBackend->PipewireConnectStream();
}

void PipewireCaptureBackend::XDPOpenPipewireRemote()
{
    GVariantBuilder builder;
    g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);

    g_dbus_proxy_call_with_unix_fd_list(this->screencastProxy, "OpenPipeWireRemote",
        g_variant_new("(oa{sv})", this->screencastSessionHandle.c_str(), &builder), G_DBUS_CALL_FLAGS_NONE, -1,
        nullptr, this->cancellable, PipewireCaptureBackend::OnPipewireRemoteOpened, this);
}

void PipewireCaptureBackend::OnStartResponse(GVariant* parameters, void* userData)
{
    PipewireCaptureBackend* captureBackend = (PipewireCaptureBackend*)userData;

    GVariant* result = nullptr;
    uint32_t response;
    g_variant_get(parameters, "(u@a{sv})", &response, &result);

    if (response != 0) {
        logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't start XDP ScreenCast, denied or cancelled by the user.\n");
        g_variant_unref(result);
        return;
    }

    // Now we can get the actual stream information and start iterating it to find our stream properties.
    GVariant* streams = g_variant_lookup_value(result, "streams", G_VARIANT_TYPE_ARRAY);
    GVariantIter iter;
    g_variant_iter_init(&iter, streams);

    size_t numStreams = g_variant_iter_n_children(&iter);
    if (numStreams != 1) {
        logCb(RETRO_LOG_WARN, "PipeWire Capture Backend: Received more than one stream from XDP."
        "This is probably a bug in the relevant desktop portal implementation.\n");

        // Workaround for buggy implementation in some versions of KDE.
        for (size_t i = 0; i < numStreams - 1; i++) {
            GVariant* throwawayProps = nullptr;
            uint32_t throwawayPipewireNode;
            g_variant_iter_loop(&iter, "(u@a{sv})", &throwawayPipewireNode, &throwawayProps);
            g_variant_unref(throwawayProps);
        }
    }

    GVariant* streamProperties;
    g_variant_iter_loop(&iter, "(u@a{sv})", &captureBackend->pipewireNode, &streamProperties);
    g_variant_unref(streamProperties);
    g_variant_unref(streams);
    g_variant_unref(result);

    // TODO: handle restore tokens here?

    logCb(RETRO_LOG_INFO, "PipeWire Capture Backend: XDP ScreenCast source selected, setting up capture.\n");

    captureBackend->XDPOpenPipewireRemote();
}

void PipewireCaptureBackend::OnStarted(GObject* source, GAsyncResult* inResult, void* userData)
{
    GVariant* result = nullptr;
    GError* error = nullptr;
    result = g_dbus_proxy_call_finish(G_DBUS_PROXY(source), inResult, &error);
    if(error) {
        if(!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
            logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't start XDP ScreenCast: %s\n", error->message);
        g_variant_unref(result);
        g_error_free(error);
        return;
    }

    g_variant_unref(result);
}

void PipewireCaptureBackend::XDPStart()
{
    std::string requestPath, requestToken;
    CreateRequestPath(&requestPath, &requestToken);

    this->startSignalSub = make_shared<XDPSignalSubscription>(this->dbusConnection, 
        requestPath.c_str(), this->cancellable,
        PipewireCaptureBackend::OnStartResponse, this);

    GVariantBuilder builder;
    g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);
    g_variant_builder_add(&builder, "{sv}", "handle_token", g_variant_new_string(requestToken.c_str()));

    g_dbus_proxy_call(this->screencastProxy, "Start", g_variant_new("(osa{sv})",
        this->screencastSessionHandle.c_str(), "", &builder), G_DBUS_CALL_FLAGS_NONE, -1,
        this->cancellable, PipewireCaptureBackend::OnStarted, this);
}

void PipewireCaptureBackend::OnSelectSourceResponse(GVariant* parameters, void* userData)
{
    PipewireCaptureBackend* captureBackend = (PipewireCaptureBackend*)userData;

    GVariant* result = nullptr;
    uint32_t response;
    g_variant_get(parameters, "(u@a{sv})", &response, &result);
    g_variant_unref(result);

    if (response != 0) {
        logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't select XDP ScreenCast source, denied or cancelled by the user.\n");
        return;
    }

    captureBackend->XDPStart();
}

void PipewireCaptureBackend::OnSourceSelected(GObject* source, GAsyncResult* inResult, void* userData)
{
    GVariant* result = nullptr;
    GError* error = nullptr;
    result = g_dbus_proxy_call_finish(G_DBUS_PROXY(source), inResult, &error);
    if(error) {
        if(!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
            logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't select XDP ScreenCast source: %s\n", error->message);
        g_variant_unref(result);
        g_error_free(error);
        return;
    }

    g_variant_unref(result);

}

void PipewireCaptureBackend::XDPSelectSource()
{
    static const uint32_t XDP_SELECT_WINDOWS = 2;
    static const uint32_t XDP_CURSOR_MODE_HIDDEN = 1;
    static const uint32_t XDP_CURSOR_MODE_EMBEDDED = 2;
    static const uint32_t XDP_PERSIST_MODE_PERSISTENT = 2;

    std::string requestPath, requestToken;
    CreateRequestPath(&requestPath, &requestToken);

    this->selectSourceSignalSub = make_shared<XDPSignalSubscription>(this->dbusConnection, 
        requestPath.c_str(), this->cancellable,
        PipewireCaptureBackend::OnSelectSourceResponse, this);

    GVariantBuilder builder;
    g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);
    g_variant_builder_add(&builder, "{sv}", "types", g_variant_new_uint32(XDP_SELECT_WINDOWS)); // Only select windows
	g_variant_builder_add(&builder, "{sv}", "multiple", g_variant_new_boolean(false));
    g_variant_builder_add(&builder, "{sv}", "handle_token", g_variant_new_string(requestToken.c_str()));

    // Get available cursor modes.
    GVariant* cachedCursorModes = nullptr;
    cachedCursorModes = g_dbus_proxy_get_cached_property(this->screencastProxy, "AvailableCursorModes");
    uint32_t availableCursorModes = cachedCursorModes ? g_variant_get_uint32(cachedCursorModes) : 0;
    g_variant_unref(cachedCursorModes);

    // TODO: handle metadata cursor mode, for now we just handle embedded if it's available
    // TODO: also maybe we want to get cursor-enable status from elsewhere?
    if (this->captureCursor && availableCursorModes & XDP_CURSOR_MODE_EMBEDDED)
        g_variant_builder_add(&builder, "{sv}", "cursor_mode", g_variant_new_uint32(XDP_CURSOR_MODE_EMBEDDED));
    else
        g_variant_builder_add(&builder, "{sv}", "cursor_mode", g_variant_new_uint32(XDP_CURSOR_MODE_HIDDEN));

    // Get ScreenCast API version.
    GVariant* cachedVersion = nullptr;
    cachedVersion = g_dbus_proxy_get_cached_property(this->screencastProxy, "version");
    uint32_t version = cachedVersion ? g_variant_get_uint32(cachedVersion) : 0;
    g_variant_unref(cachedVersion);

    // Set persist mode for version > 4.
    if (version >= 4) {
        g_variant_builder_add(&builder, "{sv}", "persist_mode", g_variant_new_uint32(XDP_PERSIST_MODE_PERSISTENT));
        // TODO: should we also be able to provide restore tokens here?
    }

    g_dbus_proxy_call(this->screencastProxy, "SelectSources",
        g_variant_new("(oa{sv})", this->screencastSessionHandle.c_str(), &builder), G_DBUS_CALL_FLAGS_NONE, -1,
        this->cancellable, PipewireCaptureBackend::OnSourceSelected, this);
}

void PipewireCaptureBackend::OnCreateSessionResponse(GVariant* parameters, void* userData)
{
    PipewireCaptureBackend* captureBackend = (PipewireCaptureBackend*)userData;

    GVariant* result = nullptr;
    uint32_t response;
    g_variant_get(parameters, "(u@a{sv})", &response, &result);

    if (response != 0) {
        logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't create XDP ScreenCast session, denied or cancelled by the user.\n");
        g_variant_unref(result);
        return;
    }

    logCb(RETRO_LOG_INFO, "PipeWire Capture Backend: XDP ScreenCast session successfully created.\n");

    GVariant* sessionHandleVariant = nullptr;
    sessionHandleVariant = g_variant_lookup_value(result, "session_handle", nullptr);
    captureBackend->screencastSessionHandle = g_variant_dup_string(sessionHandleVariant, nullptr);
    g_variant_unref(sessionHandleVariant);
    g_variant_unref(result);

    // Now start XDP ScreenCast source selection.
    captureBackend->XDPSelectSource();
}

void PipewireCaptureBackend::OnSessionCreated(GObject* source, GAsyncResult* inResult, void* userData)
{    
    GVariant* result = nullptr;
    GError* error = nullptr;
    result = g_dbus_proxy_call_finish(G_DBUS_PROXY(source), inResult, &error);
    if(error) {
        if(!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
            logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't create XDP ScreenCast session: %s\n", error->message);
        g_variant_unref(result);
        g_error_free(error);
        return;
    }

    g_variant_unref(result);
}

void PipewireCaptureBackend::XDPCreateSession()
{
    std::string requestToken, requestPath, sessionToken;
    CreateRequestPath(&requestPath, &requestToken);
    CreateSessionPath(nullptr, &sessionToken);
    
    this->createSessionSignalSub = make_shared<XDPSignalSubscription>(this->dbusConnection, 
        requestPath.c_str(), this->cancellable,
        PipewireCaptureBackend::OnCreateSessionResponse, this);

    GVariantBuilder builder;
    g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);
    g_variant_builder_add(&builder, "{sv}", "handle_token", g_variant_new_string(requestToken.c_str()));
    g_variant_builder_add(&builder, "{sv}", "session_handle_token", g_variant_new_string(sessionToken.c_str()));

    g_dbus_proxy_call(this->screencastProxy, "CreateSession", g_variant_new("(a{sv})", &builder),
        G_DBUS_CALL_FLAGS_NONE, -1, this->cancellable,
        PipewireCaptureBackend::OnSessionCreated, this);
}

void PipewireCaptureBackend::CreateNewVulkanStructures()
{
    // Try to stop the frontend from running for a bit, give us a little time to switch over.
    this->readiness = CaptureReadiness::PLEASE_WAIT;
    vkDeviceWaitIdle(hwInterface->device);

    // Create Vulkan staging buffer to match PipeWire stream.
    VkDeviceSize imageSize = this->pipewireStreamFormat.info.raw.size.width *
        this->pipewireStreamFormat.info.raw.size.height * 4;
    VulkanFrontend::CreateBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        this->stagingBuffer, this->stagingBufferMemory, "PipeWire Backend Staging Buffer");

    // Create Vulkan image to match PipeWire stream.
    VulkanFrontend::CreateImage(this->pipewireStreamFormat.info.raw.size.width,
        this->pipewireStreamFormat.info.raw.size.height, VK_FORMAT_R8G8B8A8_UNORM,
        VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->image, this->imageMemory, "PipeWire Backend Texture");

    // Create a view for the Vulkan image.
    this->imageView = VulkanFrontend::CreateImageView(this->image, VK_FORMAT_R8G8B8A8_UNORM);

    // Store the image extent.
    this->imageExtent.width = this->pipewireStreamFormat.info.raw.size.width;
    this->imageExtent.height = this->pipewireStreamFormat.info.raw.size.height;

    // And now, we are ready to rock.
    this->readiness = CaptureReadiness::READY;
}

PipewireCaptureBackend::PipewireCaptureBackend(bool captureCursor)
    : CaptureBackend({}, captureCursor), readiness(CaptureReadiness::DEAD)
{
    GError* error = nullptr;

    // Initalize PipeWire.
    pw_init(NULL, NULL);

    // Get D-Bus connection.
    this->cancellable = g_cancellable_new();
    this->dbusConnection = g_bus_get_sync(G_BUS_TYPE_SESSION, nullptr, &error);
    if(error) {
        logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't connect to D-Bus: %s\n", error->message);
        g_error_free(error);
        return;
    }

    // Get a proxy for connecting to XDG Desktop Portal ScreenCast interface.
    this->screencastProxy = g_dbus_proxy_new_sync(this->dbusConnection, G_DBUS_PROXY_FLAGS_NONE,
        nullptr, "org.freedesktop.portal.Desktop", "/org/freedesktop/portal/desktop",
        "org.freedesktop.portal.ScreenCast", nullptr, &error);
    if(error) {
        logCb(RETRO_LOG_ERROR, "PipeWire Capture Backend: Can't retrieve D-Bus proxy for XDP ScreenCast interface: %s\n", error->message);
        g_error_free(error);
        return;
    }
}

PipewireCaptureBackend::~PipewireCaptureBackend()
{
    CleanupCaptureResources();
    pw_deinit();
}

void PipewireCaptureBackend::Update(void)
{
    // Ensure GIO main context event loop is flushed.
    // Otherwise our initialization callbacks never get called.
    if (g_main_context_pending(nullptr))
        g_main_context_iteration(nullptr, true);
}

void PipewireCaptureBackend::CleanupCaptureResources()
{
    // Early bath if we aren't even set up for a capture yet.
    if(this->readiness != CaptureReadiness::READY)
        return;

    // Cleanup PipeWire.
    if(this->pipewireStream) {
        pw_thread_loop_lock(this->pipewireThreadLoop);
        pw_stream_disconnect(this->pipewireStream);
        g_clear_pointer(&this->pipewireStream, pw_stream_destroy);
        pw_thread_loop_unlock(this->pipewireThreadLoop);
    }

    // Wait for the thread loop to terminate.
    if (this->pipewireThreadLoop) {
        pw_thread_loop_wait(this->pipewireThreadLoop);
        pw_thread_loop_stop(this->pipewireThreadLoop);
    }

    g_clear_pointer(&this->pipewireContext, pw_context_destroy);
    g_clear_pointer(&this->pipewireThreadLoop, pw_thread_loop_destroy);

    if(this->pipewireFd > 0) {
        close(this->pipewireFd);
        this->pipewireFd = 0;
    }

    // Cleanup Vulkan resources.
    vkDeviceWaitIdle(hwInterface->device);

    vkDestroyFence(hwInterface->device, this->updateFence, nullptr);

    vkDestroyImageView(hwInterface->device, this->imageView, nullptr);
    vkDestroyImage(hwInterface->device, this->image, nullptr);
    vkFreeMemory(hwInterface->device, this->imageMemory, nullptr);
    vkDestroyBuffer(hwInterface->device, this->stagingBuffer, nullptr);
    vkFreeMemory(hwInterface->device, this->stagingBufferMemory, nullptr);
}

bool PipewireCaptureBackend::SetupCapture(void)
{
    // If we already have a capture ready, we need to cleanup old capture resources before we proceed.
    if(this->readiness == CaptureReadiness::READY)
        CleanupCaptureResources();

    // The final capture readiness will be set in one of the PipeWire callbacks.
    // So keep the frontend waiting for now.
    this->readiness = CaptureReadiness::PLEASE_WAIT;

    // Create the Vulkan fence, it doesn't need to be recreated on capture changes, so we can just do it here.
    VkFenceCreateInfo fenceInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
    vkCreateFence(hwInterface->device, &fenceInfo, nullptr, &updateFence);
    
    // Create the XDP ScreenCast session.
    // Everything waterfalls from the callbacks that this invokes.
    this->XDPCreateSession();

    return true;
}

CaptureBackend::CaptureReadiness PipewireCaptureBackend::GetCaptureReadiness(void)
{
    return this->readiness;
}

void PipewireCaptureBackend::UpdateCapturedFrame(void)
{
    // Wait for the staging buffer to be unlocked.
    // The actual content updates in the staging buffer are done as-and-when PipeWire calls us
    //  back with new frames (in OnProcess()).
    while(stagingBufferLock) {};

    // Get the true width and height of the valid region from PipeWire.
    uint32_t stagedWidth = this->pipewireStreamFormat.info.raw.size.width;
    uint32_t stagedHeight = this->pipewireStreamFormat.info.raw.size.height;

    // Copy the staging buffer to the real texture.
    VkCommandPool localCommandPool;
    VkCommandBuffer localCmd;
    VulkanFrontend::OpenTemporaryCommand(localCommandPool, localCmd);

    VulkanFrontend::RecordImageTransition(localCmd, this->image,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    VulkanFrontend::RecordBufferImageCopy(localCmd, stagingBuffer, this->image,
        static_cast<uint32_t>(stagedWidth), static_cast<uint32_t>(stagedHeight));
    VulkanFrontend::RecordImageTransition(localCmd, this->image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    VulkanFrontend::CloseAndSubmitCommand(localCmd, updateFence);

    vkWaitForFences(hwInterface->device, 1, &updateFence, VK_TRUE, UINT64_MAX);
    vkResetFences(hwInterface->device, 1, &updateFence);

    // Clean up.
    vkFreeCommandBuffers(hwInterface->device, localCommandPool, 1, &localCmd);
    vkDestroyCommandPool(hwInterface->device, localCommandPool, nullptr);
}

VkExtent2D PipewireCaptureBackend::GetCapturedFrameExtent(void)
{
    return this->imageExtent;
}

VkRect2D PipewireCaptureBackend::GetSuggestedCropRect(void)
{
    VkRect2D cropRect;
    cropRect.offset = {0, 0};
    cropRect.extent = {this->imageExtent.width, this->imageExtent.height};
    return cropRect;
}

bool PipewireCaptureBackend::NeedsColorSwap(void)
{
    return true;
}

VkImage PipewireCaptureBackend::GetCapturedFrame(void)
{
    return this->image;
}

VkImageView PipewireCaptureBackend::GetCapturedFrameView(void)
{
    return this->imageView;
}