#include "XCompositeCaptureBackend.h"

#include <cstring>
#include <X11/Xlib-xcb.h>
#include <X11/Xutil.h>
#include <xcb/composite.h>
#include "rapidfuzz/fuzz.hpp"
#include "VulkanFrontend.h"

extern retro_environment_t                  environCb;
extern retro_log_printf_t                   logCb;
extern retro_hw_render_interface_vulkan*    hwInterface;

#define WINDOW_RECOVERY_TIME_NSEC 20'000'000

// TODO: Implement cursor rendering support? Right now captureCursor state is ignored.

xcb_atom_t XCompositeCaptureBackend::XCBGetAtom(const char* name)
{
    xcb_intern_atom_cookie_t atomCookie =
        xcb_intern_atom(this->xcbConnection, 1, strlen(name), name);
    xcb_intern_atom_reply_t *atomReply =
        xcb_intern_atom_reply(this->xcbConnection, atomCookie, nullptr);
    xcb_atom_t atom = atomReply->atom;
    free(atomReply);
    return atom;
}

void XCompositeCaptureBackend::XCBSetupAtoms()
{
    atomUTF8_STRING = XCBGetAtom("UTF8_STRING");
    atomSTRING = XCBGetAtom("STRING");
    atomTEXT = XCBGetAtom("TEXT");
    atomCOMPOUND_TEXT = XCBGetAtom("COMPOUND_TEXT");
    atomWM_NAME = XCBGetAtom("WM_NAME");
    atomWM_CLASS = XCBGetAtom("WM_CLASS");
    atom_NET_WM_NAME = XCBGetAtom("_NET_WM_NAME");
    atom_NET_SUPPORTING_WM_CHECK = XCBGetAtom("_NET_SUPPORTING_WM_CHECK");
    atom_NET_CLIENT_LIST = XCBGetAtom("_NET_CLIENT_LIST");
}

xcb_get_property_reply_t* XCompositeCaptureBackend::XCBGetProperty(xcb_window_t window, xcb_atom_t atom)
{
    if (atom == XCB_ATOM_NONE)
        return nullptr;

    xcb_generic_error_t *error = nullptr;
    xcb_get_property_cookie_t propCookie =
        xcb_get_property(this->xcbConnection, 0, window, atom, 0, 0, 4096);
    xcb_get_property_reply_t *property =
        xcb_get_property_reply(this->xcbConnection, propCookie, &error);
    if (error != nullptr || xcb_get_property_value_length(property) == 0) {
        free(property);
        return nullptr;
    }

    return property;
}

bool XCompositeCaptureBackend::XCBCheckEWMH(xcb_window_t root)
{
    xcb_get_property_reply_t *check1 = XCBGetProperty(root, atom_NET_SUPPORTING_WM_CHECK);
    if(!check1)
        return false;

    xcb_window_t emwhWindow = ((xcb_window_t*)xcb_get_property_value(check1))[0];
    free(check1);
    xcb_get_property_reply_t *check2 = XCBGetProperty(emwhWindow, atom_NET_SUPPORTING_WM_CHECK);
    if(!check2)
        return false;

    free(check2);
    return true;
}

std::vector<xcb_window_t> XCompositeCaptureBackend::XCBGetTopLevelWindows()
{
    std::vector<xcb_window_t> windowList = {};

    if(atom_NET_CLIENT_LIST == XCB_ATOM_NONE)
        return windowList;

    xcb_screen_iterator_t screenIt =
        xcb_setup_roots_iterator(xcb_get_setup(this->xcbConnection));
    for(; screenIt.rem > 0; xcb_screen_next(&screenIt)) {
        xcb_generic_error_t *error = nullptr;
        xcb_get_property_cookie_t clientListCookie =
            xcb_get_property(this->xcbConnection, 0, screenIt.data->root,
                atom_NET_CLIENT_LIST, 0, 0, 4096);
        xcb_get_property_reply_t *clientList =
            xcb_get_property_reply(this->xcbConnection, clientListCookie, &error);
        if(error == nullptr) {
            uint32_t length = xcb_get_property_value_length(clientList) / sizeof(xcb_window_t);
            for(uint32_t i = 0; i < length; i++) {
                windowList.push_back(((xcb_window_t *)xcb_get_property_value(clientList))[i]);
            }
        }
        free(clientList);
    }

    return windowList;
}

std::string XCompositeCaptureBackend::XCBGetWindowName(xcb_window_t window)
{
    // Try using _NET_WM_NAME atom.
    xcb_get_property_reply_t *name =
        XCBGetProperty(window, atom_NET_WM_NAME);
    if (name) {
        const char* data = (const char *)xcb_get_property_value(name);
        free(name);
        return std::string(data);
    }

    // Try using WM_NAME atom.
    // TODO: Need to possibly convert between different string types. For now I just assume data is good UTF-8 text.
    name = XCBGetProperty(window, atomWM_NAME);
    if (name) {
        const char* data = (const char *)xcb_get_property_value(name);
        free(name);
        return std::string(data);
    }

    return std::string("Unknown");
}

std::string XCompositeCaptureBackend::XCBGetWindowClass(xcb_window_t window)
{
    xcb_get_property_reply_t *className =
        XCBGetProperty(window, atomWM_CLASS);
    if(!className) {
        return std::string("Unknown");
    }

    const char* data = (const char *)xcb_get_property_value(className);
    free(className);
    return std::string(data);
}

bool XCompositeCaptureBackend::XCBWindowExists(xcb_window_t window)
{
    xcb_generic_error_t *error = nullptr;
    xcb_get_window_attributes_cookie_t attribCookie =
        xcb_get_window_attributes(this->xcbConnection, window);
    xcb_get_window_attributes_reply_t *attribute =
        xcb_get_window_attributes_reply(this->xcbConnection, attribCookie, &error);

    bool exists = error == nullptr && attribute->map_state == XCB_MAP_STATE_VIEWABLE;
    free(attribute);
    return exists;
}

void XCompositeCaptureBackend::XCBSelectWindow()
{
    // Get the list of top-level windows.
    std::vector<xcb_window_t> topLevelWindows = XCBGetTopLevelWindows();

    // Score all our windows and select one.
    std::vector<std::pair<double, xcb_window_t>> scoredWindows = {};
    for(auto partial : this->capturePartials) {
        // Split capture partial from it's priority.
        std::string realPartial = partial.first;
        double priority = partial.second;

        for(auto window : topLevelWindows) {
            // Get information about the window.
            std::string windowName = XCBGetWindowName(window);
            std::string windowClass = XCBGetWindowClass(window);

            // Create lowercase comprison versions for later.
            std::string wnLowercase(windowName), wcLowercase(windowClass);
            std::transform(wnLowercase.begin(), wnLowercase.end(), wnLowercase.begin(), ::tolower);
            std::transform(wcLowercase.begin(), wcLowercase.end(), wcLowercase.begin(), ::tolower);

            // Fuzzy match on class and window names.
            double nameScore = rapidfuzz::fuzz::token_set_ratio(windowName, realPartial) + 20;
            double classScore = rapidfuzz::fuzz::token_set_ratio(windowClass, realPartial);
            double newScore = std::max(nameScore, classScore);

            // Prevent capturing RA windows themselves, this works very poorly on Linux/X11.
            if(wnLowercase.find("retroarch") != std::string::npos || wcLowercase.find("retroarch") != std::string::npos)
                continue;

            // Mix in the priority.
            newScore *= priority;

            // Log for sanity.
            logCb(RETRO_LOG_INFO, "X Composite Backend: Scoring window with name \"%s\" and class \"%s\": %f.\n",
                windowName.c_str(), windowClass.c_str(), newScore);

            // Add to scored windows.
            scoredWindows.push_back(std::pair<uint32_t, xcb_window_t>(newScore, window));
        }
    }

    // Sort scored window and select only one.
    std::sort(scoredWindows.begin(), scoredWindows.end(), [](auto& a, auto& b) {
        return a.first > b.first;
    });
    this->captureWindow = scoredWindows.front().second;
}

void XCompositeCaptureBackend::XCBRegisterWatcher()
{
    uint32_t masks = StructureNotifyMask | ExposureMask | VisibilityChangeMask;
    xcb_change_window_attributes(this->xcbConnection, this->captureWindow,
        XCB_CW_EVENT_MASK, &masks);
    xcb_composite_redirect_window(this->xcbConnection, this->captureWindow,
        XCB_COMPOSITE_REDIRECT_AUTOMATIC);
}

void XCompositeCaptureBackend::XCBUnregisterWatcher()
{
    uint32_t masks = 0;
    xcb_change_window_attributes(this->xcbConnection, this->captureWindow,
        XCB_CW_EVENT_MASK, &masks);
}

void XCompositeCaptureBackend::XCBWatcherHandleEvents(xcb_generic_event_t *event)
{
    xcb_window_t window = 0;
    switch (event->response_type & ~0x80) {
    case XCB_CONFIGURE_NOTIFY:
        window = ((xcb_configure_notify_event_t *)event)->event;
        break;
    case XCB_MAP_NOTIFY:
        window = ((xcb_map_notify_event_t *)event)->event;
        break;
    case XCB_EXPOSE:
        window = ((xcb_expose_event_t *)event)->window;
        break;
    case XCB_VISIBILITY_NOTIFY:
        window = ((xcb_visibility_notify_event_t *)event)->window;
        break;
    case XCB_DESTROY_NOTIFY:
        window = ((xcb_destroy_notify_event_t *)event)->event;
        break;
    };

    if(window == this->captureWindow)
        this->captureWindowChanged = true;
}

bool XCompositeCaptureBackend::XCBCreatePixmap()
{
    // Early bath if we don't have a window.
    if(!this->captureWindow) {
        logCb(RETRO_LOG_ERROR, "X Composite Backend: Tried to create pixmap for null window reference.\n");
        return false;
    }

    // Get window dimensions.
    xcb_generic_error_t *error = NULL;
    xcb_get_geometry_cookie_t geomCookie = xcb_get_geometry(this->xcbConnection, this->captureWindow);
    xcb_get_geometry_reply_t *geometry =
        xcb_get_geometry_reply(this->xcbConnection, geomCookie, &error);
    if(error != nullptr) {
        logCb(RETRO_LOG_ERROR, "X Composite Backend: Can't get geometry for window.\n");
        return false;
    }
    this->captureWidth = geometry->width;
    this->captureHeight = geometry->height;

    // Get the actual pixmap.
    error = nullptr;
    this->capturePixmap = xcb_generate_id(this->xcbConnection);
    xcb_void_cookie_t nameCookie =
        xcb_composite_name_window_pixmap_checked(this->xcbConnection, this->captureWindow, this->capturePixmap);
    if ((error = xcb_request_check(this->xcbConnection, nameCookie)) != nullptr) {
        logCb(RETRO_LOG_ERROR, "X Composite Backend: Can't create pixmap.\n");
        this->capturePixmap = 0;
        return false;
    }

    return true;
}

void XCompositeCaptureBackend::XCBDestroyPixmap()
{
    if(this->capturePixmap) {
        XFreePixmap(this->xDisplay, this->capturePixmap);
        this->capturePixmap = 0;
    }
}

void XCompositeCaptureBackend::CleanupCaptureResources()
{
    // Early bath if we aren't even set up for a capture yet.
    if(this->readiness != CaptureReadiness::READY)
        return;

    // Cleanup XCB resources.
    XCBDestroyPixmap();
    XCBUnregisterWatcher();

    // Cleanup Vulkan resources.
    vkDeviceWaitIdle(hwInterface->device);

    vkDestroyFence(hwInterface->device, this->updateFence, nullptr);

    vkDestroyImageView(hwInterface->device, this->imageView, nullptr);
    vkDestroyImage(hwInterface->device, this->image, nullptr);
    vkFreeMemory(hwInterface->device, this->imageMemory, nullptr);
    vkDestroyBuffer(hwInterface->device, this->stagingBuffer, nullptr);
    vkFreeMemory(hwInterface->device, this->stagingBufferMemory, nullptr);
}

XCompositeCaptureBackend::XCompositeCaptureBackend(std::vector<CapturePartial_t> inCapturePartials, bool inCaptureCursor)
    : CaptureBackend(inCapturePartials, inCaptureCursor), xcbConnGood(false),
      captureWindow(0), capturePixmap(0), captureWindowChanged(false), captureWindowLost(false),
      readiness(CaptureReadiness::DEAD)
{
    // Setup X display and XCB connections.
    this->xDisplay = XOpenDisplay(nullptr);
    this->xcbConnection = XGetXCBConnection(this->xDisplay);

    // Check for support for X Composite extension, DRI3 extension, and perform version negotiation for both.
    const xcb_query_extension_reply_t *xCompositeExt = 
        xcb_get_extension_data(this->xcbConnection, &xcb_composite_id);
    if(!xCompositeExt) {
        logCb(RETRO_LOG_ERROR, "X Composite Backend: No support for X Composite extension.\n");
        return;
    }

    xcb_composite_query_version_cookie_t compVerCookie =
        xcb_composite_query_version(this->xcbConnection, 0, 2);
    xcb_composite_query_version_reply_t *compVersion =
        xcb_composite_query_version_reply(this->xcbConnection, compVerCookie, nullptr);
    if(compVersion->major_version == 0 && compVersion->minor_version < 2) {
        logCb(RETRO_LOG_ERROR, "X Composite Backend: X Composite extension is too old, need version 0.2, server can only do version %d.%d.\n",
            compVersion->major_version, compVersion->minor_version);
        free(compVersion);
        return;
    }
    free(compVersion);

    // Setup required atoms for our operation.
    this->XCBSetupAtoms();

    // Check support for Extended Window Manager Hints (EWMH).
    xcb_screen_iterator_t it;
    int screenIdx = DefaultScreen(this->xDisplay);
    xcb_screen_t *defaultScreen = nullptr;
    it = xcb_setup_roots_iterator(xcb_get_setup(this->xcbConnection));
    for(; it.rem ; --screenIdx, xcb_screen_next(&it)) {
        if (screenIdx == 0)
            defaultScreen = it.data;
    }
    
    if(!defaultScreen || !XCBCheckEWMH(defaultScreen->root)) {
        logCb(RETRO_LOG_ERROR, "X Composite Backend: Window manager does not support Extended Window Manager Hints (EWMH).\n");
        return;
    }

    xcbConnGood = true;
}

XCompositeCaptureBackend::~XCompositeCaptureBackend()
{
    // Cleanup capture-specific XCB/Vulkan resources.
    CleanupCaptureResources();

    // Close the connection to XCB/XLib.
    XCloseDisplay(this->xDisplay);
    this->xDisplay = nullptr;
    this->xcbConnection = nullptr;
}

bool XCompositeCaptureBackend::SetupCapture()
{
    // Don't setup capture without a working XCB connection
    if(!this->xcbConnGood)
        return false;

    // If we already have a capture ready, we need to cleanup old capture resources before we proceed.
    if(this->readiness == CaptureReadiness::READY)
        CleanupCaptureResources();

    // Partials are already loaded - select the window we intend to capture.
    XCBSelectWindow();

    // Check that we actually got a valid window.
    if(!XCBWindowExists(this->captureWindow))
    {
        logCb(RETRO_LOG_ERROR, "X Composite Backend: Can't setup capture, because a visible window wasn't found at all.\n");
        return false;
    }

    // The window is valid -- marked it is unchanged.
    this->captureWindowChanged = false;
    this->captureWindowLost = false;

    // Setup pixmap for capture.
    XCBRegisterWatcher();
    if(!XCBCreatePixmap()) {
        logCb(RETRO_LOG_ERROR, "X Composite Backend: Can't setup capture, see potential reasons above.\n");
        return false;
    }

    // Create fence.
    VkFenceCreateInfo fenceInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
    vkCreateFence(hwInterface->device, &fenceInfo, nullptr, &updateFence);

    // Create permanent Vulkan staging buffer to match pixmap.
    VkDeviceSize imageSize = this->captureWidth * this->captureHeight * 4;
    VulkanFrontend::CreateBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer, stagingBufferMemory, "X Composite Backend Staging Buffer");

    // Create Vulkan image to match pixmap.
    VulkanFrontend::CreateImage(this->captureWidth, this->captureHeight, VK_FORMAT_R8G8B8A8_UNORM,
        VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->image, this->imageMemory, "X Composite Backend Texture");

    // Create a view for the Vulkan image.
    this->imageView = VulkanFrontend::CreateImageView(this->image, VK_FORMAT_R8G8B8A8_UNORM);

    // Store the image extent.
    this->imageExtent.width = this->captureWidth;
    this->imageExtent.height = this->captureHeight;

    this->readiness = CaptureReadiness::READY;
    return true;
}

CaptureBackend::CaptureReadiness XCompositeCaptureBackend::GetCaptureReadiness(void)
{    
    return this->readiness;
}

void XCompositeCaptureBackend::UpdateCapturedFrame(void)
{
    // Trigger a delayed setup of the capture if needed.
    // If window changed, we want this to repeat so that the delay keeps getting pushed forward if a bunch of changes keep happening.
    // If window lost entirely, only trigger first time.
    xcb_generic_event_t *event;
    while ((event = xcb_poll_for_event(this->xcbConnection)))
        XCBWatcherHandleEvents(event);
    bool windowLost = !XCBWindowExists(this->captureWindow);

    if(this->captureWindowChanged || (windowLost && !this->captureWindowLost)) {
        this->captureWindowChanged = false;
        this->captureWindowLost = true;
        timespec rawTime;
        clock_gettime(CLOCK_MONOTONIC_RAW, &rawTime);
        this->captureWindowRecoverTimeNsec = (rawTime.tv_sec*1'000'000'000) + rawTime.tv_nsec + WINDOW_RECOVERY_TIME_NSEC;
        return;
    }

    // If we have a lost window already, see if it's time to restart capture.
    if(this->captureWindowLost) {
        timespec currentTime;
        clock_gettime(CLOCK_MONOTONIC_RAW, &currentTime);
        uint64_t currentTimeNsec = (currentTime.tv_sec*1'000'000'000) + currentTime.tv_nsec;
        if (currentTimeNsec > this->captureWindowRecoverTimeNsec) {
            this->captureWindowLost = false;
            this->readiness = CaptureReadiness::DEAD;
        }
        return;
    }

    // TODO: Yeah this is probably not the most efficient way of doing any of this.
    //  Ideally, I'd love to be able to pass the memory backing the pixmap to Vulkan directly.
    //  But the documentation for the DRI3 extension is basically non-existent, and my first experiements with that couldn't make it work.
    //  So no idea what a 'direct handoff' will look like for now.

    // Flush display and map the pixmap as an X image.
    XFlush(this->xDisplay);
    XImage *xImage = XGetImage(this->xDisplay, this->capturePixmap,
        0, 0, this->captureWidth, this->captureHeight, AllPlanes, ZPixmap);

    // Map the staging buffer.
    VkDeviceSize imageSize = this->captureWidth * this->captureHeight * 4;
    void* data;
    vkMapMemory(hwInterface->device, stagingBufferMemory, 0, imageSize, 0, &data);
    memcpy(data, xImage->data, static_cast<size_t>(imageSize));
    vkUnmapMemory(hwInterface->device, stagingBufferMemory);

    // Free the X image -- it's not needed now the contents in the staging buffer.
    XDestroyImage(xImage);

    // Copy the staging buffer to the real texture.
    VkCommandPool localCommandPool;
    VkCommandBuffer localCmd;
    VulkanFrontend::OpenTemporaryCommand(localCommandPool, localCmd);

    VulkanFrontend::RecordImageTransition(localCmd, this->image,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    VulkanFrontend::RecordBufferImageCopy(localCmd, stagingBuffer, this->image,
        static_cast<uint32_t>(this->captureWidth), static_cast<uint32_t>(this->captureHeight));
    VulkanFrontend::RecordImageTransition(localCmd, this->image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    VulkanFrontend::CloseAndSubmitCommand(localCmd, updateFence);

    vkWaitForFences(hwInterface->device, 1, &updateFence, VK_TRUE, UINT64_MAX);
    vkResetFences(hwInterface->device, 1, &updateFence);

    // Clean up.
    vkFreeCommandBuffers(hwInterface->device, localCommandPool, 1, &localCmd);
    vkDestroyCommandPool(hwInterface->device, localCommandPool, nullptr);
}

VkExtent2D XCompositeCaptureBackend::GetCapturedFrameExtent(void)
{
    return this->imageExtent;
}

VkRect2D XCompositeCaptureBackend::GetSuggestedCropRect(void)
{
    VkRect2D cropRect;
    cropRect.offset = {0, 0};
    cropRect.extent = {this->imageExtent.width, this->imageExtent.height};
    return cropRect;
}

bool XCompositeCaptureBackend::NeedsColorSwap(void)
{
    return true;
}


VkImage XCompositeCaptureBackend::GetCapturedFrame(void)
{
    return this->image;
}

VkImageView XCompositeCaptureBackend::GetCapturedFrameView(void)
{
    return this->imageView;
}