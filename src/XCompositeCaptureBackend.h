#pragma once

#include "CaptureBackend.h"

#include <X11/Xlib.h>
#include <xcb/xcb.h>

/**
 * A capture backend based on the X Composite extension, allowing capture of a single window on X11-based Linux platforms.
 */
class XCompositeCaptureBackend : CaptureBackend
{
private:
    Display *xDisplay;
    xcb_connection_t *xcbConnection;
    bool xcbConnGood;

    xcb_atom_t atomUTF8_STRING;
    xcb_atom_t atomSTRING;
    xcb_atom_t atomTEXT;
    xcb_atom_t atomCOMPOUND_TEXT;
    xcb_atom_t atomWM_NAME;
    xcb_atom_t atomWM_CLASS;
    xcb_atom_t atom_NET_WM_NAME;
    xcb_atom_t atom_NET_SUPPORTING_WM_CHECK;
    xcb_atom_t atom_NET_CLIENT_LIST;

    xcb_window_t captureWindow;
    Pixmap capturePixmap;
    bool captureWindowChanged;
    bool captureWindowLost;
    uint64_t captureWindowRecoverTimeNsec;
    uint32_t captureWidth, captureHeight;

    VkFence updateFence;
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    VkExtent2D imageExtent;
    VkImage image;
    VkDeviceMemory imageMemory;
    VkImageView imageView;

    CaptureReadiness readiness;

    xcb_atom_t XCBGetAtom(const char* name);
    void XCBSetupAtoms();
    xcb_get_property_reply_t* XCBGetProperty(xcb_window_t window, xcb_atom_t atom);
    bool XCBCheckEWMH(xcb_window_t root);

    std::vector<xcb_window_t> XCBGetTopLevelWindows();
    std::string XCBGetWindowName(xcb_window_t window);
    std::string XCBGetWindowClass(xcb_window_t window);
    bool XCBWindowExists(xcb_window_t window);
    void XCBSelectWindow();

    void XCBRegisterWatcher();
    void XCBUnregisterWatcher();
    void XCBWatcherHandleEvents(xcb_generic_event_t *event);
    bool XCBCreatePixmap();
    void XCBDestroyPixmap();

    void CleanupCaptureResources();
public:
    /**
     * Create a new X composite capture backend.
     * @param inCapturePartials Capture partials to use when attempting to find a window.
     */
    XCompositeCaptureBackend(std::vector<CapturePartial_t> inCapturePartials, bool inCaptureCursor);
    ~XCompositeCaptureBackend();

    bool SetupCapture(void) override;
    CaptureReadiness GetCaptureReadiness(void) override;
    void UpdateCapturedFrame(void) override;
    VkExtent2D GetCapturedFrameExtent(void) override;
    VkRect2D GetSuggestedCropRect(void) override;
    bool NeedsColorSwap(void) override;
    VkImage GetCapturedFrame(void) override;
    VkImageView GetCapturedFrameView(void) override;
};