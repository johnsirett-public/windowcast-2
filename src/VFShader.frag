#version 450

layout(binding = 0) uniform sampler2D mainTexture;

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(push_constant) uniform pc {
    vec2 coordMult;
    vec2 coordShift;
    bool colorSwap;
};

void main() {
    vec4 resultColor = texture(mainTexture, fragTexCoord);
    if (colorSwap)
        outColor = resultColor.bgra;
    else
        outColor = resultColor;
}