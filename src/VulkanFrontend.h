#pragma once

#include <filesystem>
#include <vector>
#include "glm/vec2.hpp"

#include "libretro_vulkan.h"
#include "vulkan/vulkan_symbol_wrapper.h"

#define VULKAN_API_VERSION VK_API_VERSION_1_1
#define VULKAN_MAX_SYNC 8

class VulkanFrontend
{
private:
    unsigned    swapchainImageCount;
    uint32_t    swapchainMask;

    VkCommandPool   frameCommandPool[VULKAN_MAX_SYNC];
    VkCommandBuffer frameCommand[VULKAN_MAX_SYNC];
    VkFence         frameFences[VULKAN_MAX_SYNC];

    const VkFormat swapImageFormat = VK_FORMAT_R8G8B8A8_UNORM;
    uint32_t swapWidth, swapHeight;
    bool swapchainValid;
    retro_vulkan_image swapImages[VULKAN_MAX_SYNC];
    VkDeviceMemory swapImageMemory[VULKAN_MAX_SYNC];
    VkFramebuffer swapFramebuffers[VULKAN_MAX_SYNC];

    VkSampler pointSampler;
    VkSampler linearSampler;

    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout pipelineLayout;
    VkRenderPass renderPass;
    VkPipeline processPipeline;

    VkDescriptorPool descriptorPool;
    VkDescriptorSet descriptorSets[VULKAN_MAX_SYNC];

    void CreateCommonSamplers();
    void DestroyCommonSamplers();
    void CreateProcessingPipeline();
    void DestroyProcessingPipeline();
    void CreateSwapchain(uint32_t newSwapWidth, uint32_t newSwapHeight);
    void DestroySwapchain();
    void CreateDescriptors();
    void DestroyDescriptors();
public:
    /**
     * Sampling techniques which can be used for upscale/downscale.
     */
    enum class SamplingType { LINEAR, POINT };

    /**
     * Classtype for push constants used by the Vulkan Frontend's scaling shaders.
     * This structure must *exactly* match the push constants block used in shader code.
     */
    struct PushConstants {
        glm::vec2 coordMult;
        glm::vec2 coordShift;
        uint32_t colorSwap;
    };

    /**
     * Setup the Vulkan frontend, which will initalized some top-level structures needed for rendering.
     */
    VulkanFrontend(void);

    /**
     * Tear down the Vulkan frontend.
     */
    ~VulkanFrontend(void);

    /**
     * Get the swapchain mask.
     * @return The current swapchain mask.
     */
    uint32_t GetSwapchainMask(void)
    {
        return this->swapchainMask;
    }

    /**
     * Reset and open the command buffer for the specified frame.
     * @returns Per-frame VkCommandBuffer that has been prepared for recording commands.
     */
    VkCommandBuffer OpenFrameCommand(uint32_t syncIdx);

    /**
     * Request recreation of the internal swapchain images with a new resolution.
     * It is safe to call this frequently, since it only recreates the swapchain when it's extent has changed.
     * 
     * @param width New width of the swapchain images.
     * @param height New height of the swapchain images.
     */
    void RequestSwapExtent(uint32_t width, uint32_t height);

    /**
     * Blit a provided image to the screen.
     * 
     * @param image A VkImageView handle which can be used to access the image.
     * @param samplingType What sampling technqiue to use for upscale/downscale.
     * @param pushConstants Data for push constants used by the scaling shader. Expected to be filled by the caller.
     * @note The image itself doesn't need to be passed.
     */
    void BlitImage(VkImageView imageView, SamplingType samplingType,PushConstants pushConstants);

    /**
     * Returns WindowCast's static Vulkan application info.
     * @return Pointer to static VkApplicationInfo struct containing Vulkan application info.
     */
    static const VkApplicationInfo* GetApplicationInfo(void);

    /**
     * Called by LR/RA to create the Vulkan instance. Not to be called from other code.
     * This is where the core can ensure all needed extensions / layers are enabled.
     * 
     * @note All parameters are LR/RA provided context and/or callbacks.
     * @return The newly created VkInstance.
     */
    static VkInstance CreateInstance(
        PFN_vkGetInstanceProcAddr get_instance_proc_addr,
        const VkApplicationInfo *app,
        retro_vulkan_create_instance_wrapper_t create_instance_wrapper,
        void *opaque);

    /**
     * Called by LR/RA to create additonal Vulkan context. Not to be called from other code.
     * The created context includes:
     *      - A VkPhysicalDevice representing one of the system GPUs.
     *      - A VkDevice, or logical device, allowing commands to be run on that GPU.
     *      - A pair of VkQueues for submitting commands to that GPU;
     *          - A main queue, supporting graphics and compute commands.
     *          - A presentation queue, supporting presentation. This may be the same queue.
     * 
     * This function is also responsible for ensuring all device extensions needed by the core are enabled.
     * 
     * @param context A retro_vulkan_context structure which the core will fill out.
     * @note All other parameters are LRsetup/RA provided context and/or callbacks.
     * @return A boolean indicating device creation success/failure.
     */
    static bool CreateDevice(
        struct retro_vulkan_context *context,
        VkInstance instance,
        VkPhysicalDevice gpu,
        VkSurfaceKHR surface,
        PFN_vkGetInstanceProcAddr get_instance_proc_addr,
        retro_vulkan_create_device_wrapper_t create_device_wrapper,
        void *opaque);

    /**
     * Utility to identify a suitable Vulkan memory type for a set of memory requirements.
     * 
     * @param typeMask Mask of memory types that are suitable for the requriements.
     * @param hostReqs Additional property requirements for the memory type.
     * @return Index of a suitable memory type.
     */
    static uint32_t IdentifyMemoryType(uint32_t typeMask, uint32_t hostReqs);

    /**
     * Utility to create and allocate memory to Vulkan buffers of various types.
     * 
     * @param size Size of the required buffer.
     * @param usage Usage flags for the created buffer.
     * @param properties Memory properties needed for the memory bound to this buffer.
     * @param buffer Output paramter for the newly created VkBuffer handle representing the buffer.
     * @param bufferMemory Output parameter for a VkDeviceMemory handle representing the memory bound to this buffer.
     * @param name Optional name for the created buffer handle (for debugging).
     */
    static void CreateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
        VkBuffer& buffer, VkDeviceMemory& bufferMemory, const char* name = nullptr);

    /**
     * Utility to create and allocate memory to 2D Vulkan images.
     * 
     * @param width Width of the created image.
     * @param height height of the created image.
     * @param usage Usage flags for the created image.
     * @param properties Memory properties needed for the memory bound to this image.
     * @param image Output paramter for the newly created VkBuffer handle representing the image.
     * @param imageMemory Output parameter for a VkDeviceMemory handle representing the memory bound to this image.
     * @param name Optional name for the created image handle (for debugging).
     * @param flags Additonal Vulkan flags for the image.
     */
    static void CreateImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
        VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage &image, VkDeviceMemory &imageMemory,
        const char* name = nullptr, VkImageCreateFlags flags = 0);

    /**
     * Utility to create basic image views over 2D Vulkan images.
     * 
     * @param image The VkImage to creatsetupe an image for.
     * @param format The format of the image view to create.
     *  This should either not differ from the known format of the image, or the image should be created
     *  with mutable format support.
     * @param infoOut Optionally allows the return of the info used to create the image.
     */
    static VkImageView CreateImageView(VkImage image, VkFormat format, VkImageViewCreateInfo *infoOut = nullptr);

    /**
     * Utility to load a file containing SPIR-V bytecode and create a VkShaderModule from it.
     * @return The newly created VkShaderModule.
     */
    static VkShaderModule CreateShaderModuleFromFile(const std::filesystem::path& filepath);

    /**
     * Create and allocate a new temporary command pool and command buffer for use code outside the Vulkan frontend.
     * The command buffer in question is intended for one-time use.
     * Then open the new command buffer.
     * This code is expected to close this pool and buffer itself.
     * @param commandPool The newly created VkCommandPool.
     * @param command The newly create VkCommandBuffer.
     */
    static void OpenTemporaryCommand(VkCommandPool& commandPool, VkCommandBuffer& command);

    /**
     * Close the provided command buffer and submit it to our graphics queue.
     * @param command The VkCommandBuffer to be submitted.
     * @param fence A VkFence to signal once the queue submission is complete. May be omitted.
     */
    static void CloseAndSubmitCommand(VkCommandBuffer command, VkFence fence = nullptr);

    /**
     * Record a command to transition the layout of an image.
     * @param command VkCommandBuffer to record the command into.
     * @param image VkImage to transition the layout of.
     * @param oldLayout Existing layout of the image before transition.
     * @param newLayout Required layout of the image after transition.
     */
    static void RecordImageTransition(VkCommandBuffer command, VkImage image,
        VkImageLayout oldLayout, VkImageLayout newLayout);

    /**
     * Record a command to copy a buffer to an image.
     * @param buffer The VkBuffer to use as source.
     * @param image The VkImage to use as destination.
     * @param width Width of the destination image.
     * @param height Height of the destination image.
     */
    static void RecordBufferImageCopy(VkCommandBuffer command, VkBuffer buffer, VkImage image,
        uint32_t width, uint32_t height);

    /**
     * Record a command to copy an image to an image.
     * @param srcImage The VkBuffer to use as source.
     * @param dstImage The VkImage to use as destination.
     * @param width Width of the destination image.
     * @param height Height of the destination image.
     */
    static void RecordImageCopy(VkCommandBuffer command, VkImage srcImage, VkImage dstImage,
        uint32_t width, uint32_t height);

    /**
     * Record a commmand to start a render pass.
     * @param renderPass The render pass to start.
     * @param framebuffer Framebuffer object describing all attachments for use during the render pass.
     * @param clearValues Clear values for attachements which require clears.
     */
    static void RecordRenderPassStart(VkCommandBuffer command, VkRenderPass renderPass,
        VkFramebuffer framebuffer, uint32_t width, uint32_t height,
        std::vector<VkClearValue> clearValues = std::vector<VkClearValue>());
};