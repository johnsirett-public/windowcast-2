#pragma once

#include <string>
#include <utility>
#include <vector>

#include "libretro_vulkan.h"

/**
 * A capture partial combines a string (the actual partial) with a priority value.
 */
typedef std::pair<std::string, double> CapturePartial_t;

/**
 * This abstract class represents a capture backend, the part of WindowCast which talks via OS APIs to obtain captured frames.
 * 
 * Since almost everything related to both selecting windows and capturing frames will be platform/API specific,
 * the only generic thing stored in this class is the list of capture partials used to help select windows.
 * Window handles, and any other structures should be created as private members in API-specific subclasses.
 * 
 * It is expected that the subclass at least store one retro_vulkan_image and it's extent as a VkExtent2D.
 * 
 * The frontend should call SetupCapture() before captured frames are delivered.
 * This should be ideally called in retro_reset().
 * 
 * The frontend should first check whether a capture is ready via GetCaptureReadiness().
 * Once a capture is ready the process for the frontend collecting an updated frame is threefold.
 *      - Call UpdateCaptureFrame() once to update to the latest frame. The backend may capture asynchronously,
 *          but should offer a best effort gurantee to update endpoint internal structures when this is called.
 *      - Call GetCapturedFrameExtent() to get the extent of the captured frame for passing back to libretro.
 *      - Call GetCapturedFrame() to get a retro_vulkan_image structure for the captured frame.
 *          This includes both the image handle itself, an image view, and creation info about that image view.
 * 
 * The image returned by GetCapturedFrame should be in the VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL layout.
 */
class CaptureBackend
{
protected:
    std::vector<CapturePartial_t> capturePartials;
    bool captureCursor;
public:
    /**
     * Indicates the readiness of the underlying capture. The following states might be reported:
     * 
     * PLEASE_WAIT: The frontend should wait, as the capture is not yet set up.
     *      Don't call UpdatedCapturedFrame() or SetupCapture().
     * READY: A capture is ready. It should be safe to call UpdatedCapturedFrame().
     * DEAD: The capture is dead, and will not be revived (or setup has no yet been triggered).
     *      It should be safe for the frontend to call SetupCapture().
     */
    enum class CaptureReadiness { DEAD, PLEASE_WAIT, READY };

    /**
     * Create a new capture backend, storing capture partials.
     * @param inCapturePartials Capture partials to use when attempting to find a window.
     * @param inCaptureCursor Flag indicating whether to capture the cursor.
     */
    CaptureBackend(std::vector<CapturePartial_t> inCapturePartials, bool inCaptureCursor)
        : capturePartials(inCapturePartials), captureCursor(inCaptureCursor)
    {}

    /**
     * Tear-down capture backend.
    */
    virtual ~CaptureBackend() = default;

    /**
     * Unconditional update function.
     * 
     * This shoulbe be called by the frontend every frame, even if the capture is dead.
     * It allows the backend to complete ongoing processes required for it's operation and initalization,
     * such as pumping private event loops.
     */
    virtual void Update(void) {};

    /**
     * Setup the capture.
     * 
     * This will trigger the capture backend to attempt to find a window to capture based on the stored partials,
     * as well as (re-)create all internal structures needed for the capture.
     * The frontend may call this at any time, and it is the backend's responsibility to ensure it is reentrancy-safe.
     * 
     * @return Boolean indiciating success/failure of setting up the capture.
     */
    virtual bool SetupCapture(void) = 0;

    /**
     * Get the readiness of the underlying capture.
     * @return CaptureReadiness representing the current readiness of the underlying capture.
     */
    virtual CaptureReadiness GetCaptureReadiness(void) = 0;

    /**
     * Update the contents of the captured frame.
     * This function may recreate an internal VkImage handle, so the frontend should not cache this;
     *      it should always get an up-to-date handle via GetCapturedFrame().
     */
    virtual void UpdateCapturedFrame(void) = 0;

    /**
     * Get the extent of the currently updated captured frame.
     * This function always returns the total extent of the raw uncropped frame.
     * This is frame-related data, so should only be obtained after a call to UpdateCapturedFrame().
     * 
     * @return VkExtent2D representing the captured frame.
     */
    virtual VkExtent2D GetCapturedFrameExtent(void) = 0;

    /**
     * Get a cropping rectangle suggested by this backend.
     * This, for example, might be the cropping rectangle needed to crop window decorations from the captured frame.
     * This is frame-related data, so should only be obtained after a call to UpdateCapturedFrame().
     * 
     * @return VkRect2D describing the cropping rectangle.
     */
    virtual VkRect2D GetSuggestedCropRect(void) = 0;

    /**
     * Get a boolean indicating whether this backend needs the frontend to translate RGB to BGR colors.
     * 
     * @return True if color swap is needed, false otherwise.
     */
    virtual bool NeedsColorSwap(void) = 0;

    /**
     * Get a VkImage referring to the currently updated captured frame.
     * The image should have been transitioned into layout VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL.
     * This is frame-related data, so should only be obtained after a call to UpdateCapturedFrame().
     * 
     * @return VkImage representing the captured frame.
     */
    virtual VkImage GetCapturedFrame(void) = 0;

    /**
     * Get a VkImageView over the currently updated captured frame.
     * This is frame-related data, so should only be obtained after a call to UpdateCapturedFrame().
     * 
     * @return VkImageView representing the captured frame.
     */
    virtual VkImageView GetCapturedFrameView(void) = 0;
};