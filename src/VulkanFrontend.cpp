#include "VulkanFrontend.h"

#include <cstring>
#include <fstream>
#include <optional>
#include <set>
#include <string>

extern retro_environment_t                  environCb;
extern retro_video_refresh_t                videoCb;
extern retro_log_printf_t                   logCb;
extern retro_hw_render_interface_vulkan*    hwInterface;

#define SWAP_NOMINAL_WIDTH 1280
#define SWAP_NOMINAL_HEIGHT 720

void VulkanFrontend::CreateSwapchain(uint32_t newSwapWidth, uint32_t newSwapHeight)
{
    this->DestroySwapchain();

    // Save the new swap extent.
    swapWidth = newSwapWidth;
    swapHeight = newSwapHeight;
    
    // (Re)-create swapchain images.
    for (unsigned i = 0; i < this->swapchainImageCount; i++)
    {
        // Create a unique name for this image.
        std::string imageName("Swapchain Image ");
        imageName += std::to_string(i);

        // Create the actual image using our helper.
        CreateImage(swapWidth, swapHeight, VulkanFrontend::swapImageFormat,
            VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            this->swapImages[i].create_info.image, this->swapImageMemory[i], imageName.c_str(), VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT);

        // Create image views and set layout for RA image.
        this->swapImages[i].image_view = CreateImageView(this->swapImages[i].create_info.image, VulkanFrontend::swapImageFormat,
            &this->swapImages[i].create_info);
        this->swapImages[i].image_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        // Create framebuffers for these image views so we can render direct to this images ourselves.
        VkFramebufferCreateInfo framebufferInfo = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO };
        framebufferInfo.renderPass = this->renderPass;
        framebufferInfo.attachmentCount = 1;
        framebufferInfo.pAttachments = &this->swapImages[i].image_view;
        framebufferInfo.width = newSwapWidth;
        framebufferInfo.height = newSwapHeight;
        framebufferInfo.layers = 1;

        vkCreateFramebuffer(hwInterface->device, &framebufferInfo, nullptr, &this->swapFramebuffers[i]);
    }

    swapchainValid = true;
}

void VulkanFrontend::DestroySwapchain()
{
    if(!swapchainValid)
        return;

    // Cleanup swapchain image structures.
    for (unsigned i = 0; i < this->swapchainImageCount; i++)
    {
        vkDestroyFramebuffer(hwInterface->device, this->swapFramebuffers[i], nullptr);
        vkDestroyImageView(hwInterface->device, this->swapImages[i].image_view, nullptr);
        vkFreeMemory(hwInterface->device, this->swapImageMemory[i], nullptr);
        vkDestroyImage(hwInterface->device, this->swapImages[i].create_info.image, nullptr);
    }
}

void VulkanFrontend::CreateProcessingPipeline()
{
    // Load shaders from the system directory.
    const char* systemPathRaw;
    environCb(RETRO_ENVIRONMENT_GET_SYSTEM_DIRECTORY, &systemPathRaw);
    std::filesystem::path systemPath = std::filesystem::path(systemPathRaw);
    systemPath /= std::filesystem::path("windowcast");

    VkShaderModule vertShaderModule = VulkanFrontend::CreateShaderModuleFromFile(systemPath / "vert.spv");
    VkShaderModule fragShaderModule = VulkanFrontend::CreateShaderModuleFromFile(systemPath / "frag.spv");

    // Create shader stages.
    VkPipelineShaderStageCreateInfo vertShaderStageInfo = { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO };
    vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module = vertShaderModule;
    vertShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo fragShaderStageInfo = { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO };
    fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module = fragShaderModule;
    fragShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

    // Create other fixed state.
    VkPipelineVertexInputStateCreateInfo vertexInputInfo = { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO };

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo = { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO };
    inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;

    std::vector<VkDynamicState> dynamicStates = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };
    VkPipelineDynamicStateCreateInfo dynStateInfo = { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO };
    dynStateInfo.dynamicStateCount = dynamicStates.size();
    dynStateInfo.pDynamicStates = dynamicStates.data();

    VkPipelineViewportStateCreateInfo viewportStateInfo = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO };
    viewportStateInfo.viewportCount = 1;
    viewportStateInfo.scissorCount = 1;

    VkPipelineRasterizationStateCreateInfo rasterizerInfo = { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO };
    rasterizerInfo.depthClampEnable = VK_FALSE;
    rasterizerInfo.rasterizerDiscardEnable = VK_FALSE;
    rasterizerInfo.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizerInfo.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizerInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterizerInfo.depthBiasEnable = VK_FALSE;
    rasterizerInfo.lineWidth = 1.0f;

    VkPipelineMultisampleStateCreateInfo multisamplingInfo = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO };
    multisamplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisamplingInfo.sampleShadingEnable = VK_FALSE;

    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo colorBlendInfo = { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO };
    colorBlendInfo.logicOpEnable = VK_FALSE;
    colorBlendInfo.attachmentCount = 1;
    colorBlendInfo.pAttachments = &colorBlendAttachment;

    // Create descriptor set layout and pipeline layout.
    // We want a layout that provides us one sampler binding at uniform 0.
    // We shouldn't need anything else.
    VkDescriptorSetLayoutBinding samplerBinding = {};
    samplerBinding.binding = 0;
    samplerBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerBinding.descriptorCount = 1;
    samplerBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    samplerBinding.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo layoutInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO };
    layoutInfo.bindingCount = 1;
    layoutInfo.pBindings = &samplerBinding;
    vkCreateDescriptorSetLayout(hwInterface->device, &layoutInfo, nullptr, &this->descriptorSetLayout);

    VkPushConstantRange pcRange = {};
    pcRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    pcRange.offset = 0;
    pcRange.size = sizeof(VulkanFrontend::PushConstants);

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO };
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
    pipelineLayoutInfo.pushConstantRangeCount = 1;
    pipelineLayoutInfo.pPushConstantRanges = &pcRange;
    vkCreatePipelineLayout(hwInterface->device, &pipelineLayoutInfo, nullptr, &this->pipelineLayout);

    // Create render pass.
    VkAttachmentDescription colorAttachment = {};
    colorAttachment.format = VulkanFrontend::swapImageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    
    VkRenderPassCreateInfo renderPassInfo = { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO };
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    
    vkCreateRenderPass(hwInterface->device, &renderPassInfo, nullptr, &this->renderPass);

    // Create graphics pipeline.
    VkGraphicsPipelineCreateInfo pipelineInfo = { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO };
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStages;
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssemblyInfo;
    pipelineInfo.pViewportState = &viewportStateInfo;
    pipelineInfo.pRasterizationState = &rasterizerInfo;
    pipelineInfo.pMultisampleState = &multisamplingInfo;
    pipelineInfo.pColorBlendState = &colorBlendInfo;
    pipelineInfo.pDynamicState = &dynStateInfo;
    pipelineInfo.layout = this->pipelineLayout;
    pipelineInfo.renderPass = this->renderPass;
    pipelineInfo.subpass = 0;
    
    vkCreateGraphicsPipelines(hwInterface->device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &this->processPipeline);

    // Clean up.
    vkDestroyShaderModule(hwInterface->device, vertShaderModule, nullptr);
    vkDestroyShaderModule(hwInterface->device, fragShaderModule, nullptr);
}

void VulkanFrontend::DestroyProcessingPipeline()
{
    vkDestroyPipeline(hwInterface->device, this->processPipeline, nullptr);
    vkDestroyRenderPass(hwInterface->device, this->renderPass, nullptr);
    vkDestroyPipelineLayout(hwInterface->device, this->pipelineLayout, nullptr);
    vkDestroyDescriptorSetLayout(hwInterface->device, this->descriptorSetLayout, nullptr);
}

void VulkanFrontend::CreateCommonSamplers()
{
    // Create point sampler.
    VkSamplerCreateInfo samplerInfo = { VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO };
    samplerInfo.magFilter = VK_FILTER_NEAREST;
    samplerInfo.minFilter = VK_FILTER_NEAREST;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.anisotropyEnable = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 0.0f;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;

    vkCreateSampler(hwInterface->device, &samplerInfo, nullptr, &this->pointSampler);

    // Create linear sampler.
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;

    vkCreateSampler(hwInterface->device, &samplerInfo, nullptr, &this->linearSampler);
}

void VulkanFrontend::DestroyCommonSamplers()
{
    vkDestroySampler(hwInterface->device, this->linearSampler, nullptr);
    vkDestroySampler(hwInterface->device, this->pointSampler, nullptr);
}

void VulkanFrontend::CreateDescriptors()
{
    // For now, create only a single descriptor pool, for combined image samplers.
    VkDescriptorPoolSize poolSize = {};
    poolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSize.descriptorCount = VULKAN_MAX_SYNC;

    VkDescriptorPoolCreateInfo poolInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO };
    poolInfo.maxSets = VULKAN_MAX_SYNC;
    poolInfo.poolSizeCount = 1;
    poolInfo.pPoolSizes = &poolSize;

    vkCreateDescriptorPool(hwInterface->device, &poolInfo, nullptr, &this->descriptorPool);

    // And the sets themselves.
    std::vector<VkDescriptorSetLayout> layouts(VULKAN_MAX_SYNC, this->descriptorSetLayout);
    VkDescriptorSetAllocateInfo allocInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO };
    allocInfo.descriptorPool = this->descriptorPool;
    allocInfo.descriptorSetCount = VULKAN_MAX_SYNC;
    allocInfo.pSetLayouts = layouts.data();

    vkAllocateDescriptorSets(hwInterface->device, &allocInfo, this->descriptorSets);
}

void VulkanFrontend::DestroyDescriptors()
{
    vkDestroyDescriptorPool(hwInterface->device, this->descriptorPool, nullptr);
}

VulkanFrontend::VulkanFrontend(void) : swapWidth(0), swapHeight(0), swapchainValid(false)
{
    // Figure out how many swapchain images we have, and save the swapchain mask.
    unsigned imageCount = 0;
    uint32_t mask = hwInterface->get_sync_index_mask(hwInterface->handle);
    for (unsigned i = 0; i < 32; i++)
        if (mask & (1u << i))
            imageCount = i + 1;
    this->swapchainImageCount = imageCount;
    this->swapchainMask = mask;

    // Create our per-frame command pools and command buffers.
    VkCommandPoolCreateInfo poolInfo = { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO };
    VkCommandBufferAllocateInfo bufferInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO };
    poolInfo.queueFamilyIndex = hwInterface->queue_index;
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    bufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    bufferInfo.commandBufferCount = 1;
    for (unsigned i = 0; i < this->swapchainImageCount; i++) {
        vkCreateCommandPool(hwInterface->device, &poolInfo, nullptr, &this->frameCommandPool[i]);
        bufferInfo.commandPool = this->frameCommandPool[i];
        vkAllocateCommandBuffers(hwInterface->device, &bufferInfo, &this->frameCommand[i]);
    }

    // Create per-frame fences.
    VkFenceCreateInfo fenceInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    for (unsigned i = 0; i < this->swapchainImageCount; i++) {
        vkCreateFence(hwInterface->device, &fenceInfo, nullptr, &this->frameFences[i]);
    }

    // Create common samplers.
    CreateCommonSamplers();

    // Create the processing pipeline.
    CreateProcessingPipeline();

    // Create the swapchain with a nominal size.
    CreateSwapchain(SWAP_NOMINAL_WIDTH, SWAP_NOMINAL_HEIGHT);

    // Create descriptor pools and sets.
    CreateDescriptors();
}

VulkanFrontend::~VulkanFrontend(void)
{
    if(!hwInterface)
        return;

    // Wait for device to go idle before cleanup.
    vkDeviceWaitIdle(hwInterface->device);

    // Cleanup descriptor pools and sets.
    DestroyDescriptors();

    // Cleanup the pipeline.
    DestroyProcessingPipeline();

    // Cleanup common samplers.
    DestroyCommonSamplers();

    // Cleanup the swapchain.
    DestroySwapchain();

    // Cleanup command pools, buffers and fences.
    for (unsigned i = 0; i < this->swapchainImageCount; i++) {
        vkFreeCommandBuffers(hwInterface->device, this->frameCommandPool[i], 1, &this->frameCommand[i]);
        vkDestroyCommandPool(hwInterface->device, this->frameCommandPool[i], nullptr);
        vkDestroyFence(hwInterface->device, this->frameFences[i], nullptr);
    }
}

VkCommandBuffer VulkanFrontend::OpenFrameCommand(uint32_t syncIdx)
{
    // Update our sync index and get the appropriate command buffer.
    VkCommandBuffer command = this->frameCommand[syncIdx];

    // Start recording command buffer for this frame.
    VkCommandBufferBeginInfo beginInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    vkResetCommandBuffer(command, 0);
    vkBeginCommandBuffer(command, &beginInfo);

    return command;
}

void VulkanFrontend::RequestSwapExtent(uint32_t width, uint32_t height)
{
    if(width != swapWidth || height != swapHeight)
        CreateSwapchain(width, height); // The new width and height will be saved by this function.
}

void VulkanFrontend::BlitImage(VkImageView imageView, SamplingType samplingType, PushConstants pushConstants)
{
    // Get the current frame's sync index.
    uint32_t syncIdx = hwInterface->get_sync_index(hwInterface->handle);

    // Early bath if frame already in flight.
    if(vkWaitForFences(hwInterface->device, 1, &this->frameFences[syncIdx], VK_TRUE, 0) == VK_TIMEOUT) {
        return;
    }

    // Update descriptor sets to load the image into the sampler.
    VkDescriptorImageInfo imageInfo{};
    if(samplingType == SamplingType::POINT)
        imageInfo.sampler = this->pointSampler;
    else
        imageInfo.sampler = this->linearSampler;
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfo.imageView = imageView;

    VkWriteDescriptorSet descriptorWrite = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
    descriptorWrite.dstSet = this->descriptorSets[syncIdx];
    descriptorWrite.dstBinding = 0;
    descriptorWrite.dstArrayElement = 0;
    descriptorWrite.descriptorCount = 1;
    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrite.pImageInfo = &imageInfo;

    vkUpdateDescriptorSets(hwInterface->device, 1, &descriptorWrite, 0, nullptr);

    // Open the frame command buffer.
    VkCommandBuffer localCmd = this->OpenFrameCommand(syncIdx);

    // COMMAND - Start the render pass for FS quad process/draw.
    VulkanFrontend::RecordRenderPassStart(localCmd, this->renderPass, this->swapFramebuffers[syncIdx],
        swapWidth, swapHeight);
    
    // COMMAND - Bind the GFX pipeline.
    vkCmdBindPipeline(localCmd, VK_PIPELINE_BIND_POINT_GRAPHICS, this->processPipeline);

    // COMMAND - Set the viewport.
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = swapWidth;
    viewport.height = swapHeight;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    vkCmdSetViewport(localCmd, 0, 1, &viewport);

    // COMMAND - Set scissor.
    VkRect2D scissor = {};
    scissor.offset = {0, 0};
    scissor.extent = {swapWidth, swapHeight};
    vkCmdSetScissor(localCmd, 0, 1, &scissor);

    // COMMAND - Bind descriptor sets.
    vkCmdBindDescriptorSets(localCmd, VK_PIPELINE_BIND_POINT_GRAPHICS, this->pipelineLayout,
        0, 1, &this->descriptorSets[syncIdx], 0, nullptr);

    // COMMAND - Fill push constants.
    vkCmdPushConstants(localCmd, this->pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
        0, sizeof(pushConstants), &pushConstants);

    // COMMAND - Draw the FS quad. Go!
    vkCmdDraw(localCmd, 6, 1, 0, 0);

    // COMMAND - End the render pass.
    vkCmdEndRenderPass(localCmd);

    // Close and submit the frame command buffer.
    VulkanFrontend::CloseAndSubmitCommand(localCmd, this->frameFences[syncIdx]);

    // Trigger further rendering by calling back to RetroArch.
    hwInterface->set_image(hwInterface->handle, &this->swapImages[syncIdx], 0, nullptr, VK_QUEUE_FAMILY_IGNORED);
    videoCb(RETRO_HW_FRAME_BUFFER_VALID, this->swapWidth, this->swapHeight, 0);
}

const VkApplicationInfo* VulkanFrontend::GetApplicationInfo(void)
{
    static VkApplicationInfo info = {
        VK_STRUCTURE_TYPE_APPLICATION_INFO, nullptr,
        "WindowCast 2 for LibRetro", 2, nullptr, 0,
        VULKAN_API_VERSION
    };
    return &info;
}

VkInstance VulkanFrontend::CreateInstance(
        PFN_vkGetInstanceProcAddr get_instance_proc_addr,
        const VkApplicationInfo *app,
        retro_vulkan_create_instance_wrapper_t create_instance_wrapper,
        void *opaque)
{
    VkInstanceCreateInfo createInfo = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };
    createInfo.pApplicationInfo = app;
    std::vector<const char*> extensions = { VK_EXT_DEBUG_UTILS_EXTENSION_NAME };
    createInfo.enabledExtensionCount = extensions.size();
    createInfo.ppEnabledExtensionNames = extensions.data();
    // Uncomment these next 3 lines to force enable validation layers if needed.
    // This is not needed for debug-built RA, which enables validation layers itself.
    //std::vector<const char*> layers = { "VK_LAYER_KHRONOS_validation" };
    //createInfo.enabledLayerCount = layers.size();
    //createInfo.ppEnabledLayerNames = layers.data();
    return create_instance_wrapper(opaque, &createInfo);
}

bool VulkanFrontend::CreateDevice(
        struct retro_vulkan_context *context,
        VkInstance instance,
        VkPhysicalDevice gpu,
        VkSurfaceKHR surface,
        PFN_vkGetInstanceProcAddr get_instance_proc_addr,
        retro_vulkan_create_device_wrapper_t create_device_wrapper,
        void *opaque)
{
    // Preload symbols for device creation.
    vulkan_symbol_wrapper_init(get_instance_proc_addr);
    vulkan_symbol_wrapper_load_core_instance_symbols(instance);
    vulkan_symbol_wrapper_load_instance_symbol(instance, "vkGetPhysicalDeviceSurfaceSupportKHR", (PFN_vkVoidFunction*)&vkGetPhysicalDeviceSurfaceSupportKHR);
    vulkan_symbol_wrapper_load_instance_symbol(instance, "vkSetDebugUtilsObjectNameEXT", (PFN_vkVoidFunction*)&vkSetDebugUtilsObjectNameEXT);

    // Find suitable queue families for rendering and presentation.
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, queueFamilies.data());

    std::optional<uint32_t> mainQFIdx, presentQFIdx;
    for (unsigned i = 0; i < queueFamilies.size(); i++) {
        if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT && queueFamilies[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
            mainQFIdx = i;

        VkBool32 supportsPresent = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(gpu, i, surface, &supportsPresent);
        if (supportsPresent)
            presentQFIdx = i;

        if (mainQFIdx.has_value() && presentQFIdx.has_value())
            break;
    }

    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    std::set<uint32_t> uniqueQueueFamilies = { mainQFIdx.value(), presentQFIdx.value() };
    float queuePriority = 1.0f;
    for (uint32_t queueFamily : uniqueQueueFamilies) {
        VkDeviceQueueCreateInfo queueCreateInfo = { VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO };
        queueCreateInfo.queueFamilyIndex    = queueFamily;
        queueCreateInfo.queueCount          = 1;
        queueCreateInfo.pQueuePriorities    = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    // Determine required extensions for this platform.
#ifdef _WIN32
    std::vector<const char*> requiredExtensions = { "VK_KHR_external_memory_win32" };
#else
    std::vector<const char*> requiredExtensions = {};
#endif

    // Check supported extensions.
    uint32_t deviceExtensionCount;
    vkEnumerateDeviceExtensionProperties(gpu, nullptr, &deviceExtensionCount, nullptr);
    std::vector<VkExtensionProperties> deviceExtensionProps(deviceExtensionCount);
    vkEnumerateDeviceExtensionProperties(gpu, nullptr, &deviceExtensionCount, deviceExtensionProps.data());

    for (auto reqExtension : requiredExtensions)
    {
        bool extensionFound = false;

        for (auto deviceExtension : deviceExtensionProps)
        {
            if (!strcmp(deviceExtension.extensionName, reqExtension)) {
                extensionFound = true; break;
            }
        }

        if (!extensionFound) {
            logCb(RETRO_LOG_ERROR, "WindowCast 2 requires support for the %s extension on this platform.", reqExtension);
            environCb(RETRO_ENVIRONMENT_SHUTDOWN, nullptr);
            return false;
        }
    }

    // Setup device and get queues.
    VkPhysicalDeviceFeatures pdf = { 0 };
    VkDeviceCreateInfo createInfo = { VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO };
    VkQueue mainQueue, presentQueue;
    createInfo.queueCreateInfoCount     = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos        = queueCreateInfos.data();
    createInfo.pEnabledFeatures         = &pdf;
    createInfo.enabledExtensionCount    = requiredExtensions.size();
    createInfo.ppEnabledExtensionNames  = requiredExtensions.data();
    VkDevice device = create_device_wrapper(gpu, opaque, &createInfo);
    
    vulkan_symbol_wrapper_load_core_device_symbols(device);
    
    vkGetDeviceQueue(device, mainQFIdx.value(), 0, &mainQueue);
    vkGetDeviceQueue(device, presentQFIdx.value(), 0, &presentQueue);

    // Return created context to LR/RA.
    context->gpu                                = gpu;
    context->device                             = device;
    context->queue                              = mainQueue;
    context->queue_family_index                 = mainQFIdx.value();
    context->presentation_queue                 = presentQueue;
    context->presentation_queue_family_index    = presentQFIdx.value();
    return true;
}

uint32_t VulkanFrontend::IdentifyMemoryType(uint32_t typeMask, uint32_t hostReqs)
{
    // Get GPU memory properties.
    VkPhysicalDeviceMemoryProperties memoryProps;
    vkGetPhysicalDeviceMemoryProperties(hwInterface->gpu, &memoryProps);

    // Find a suitable memory type per our requirements.
    for (unsigned i = 0; i < VK_MAX_MEMORY_TYPES; i++)
    {
        if (typeMask & (1u << i))
        {
            if ((memoryProps.memoryTypes[i].propertyFlags & hostReqs) == hostReqs)
            {
                return i;
            }
        }
    }
    return 0;
}

void VulkanFrontend::CreateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
    VkBuffer& buffer, VkDeviceMemory& bufferMemory, const char* name)
{
    // Create the buffer handle.
    VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    vkCreateBuffer(hwInterface->device, &bufferInfo, nullptr, &buffer);

    // Name the buffer.
    if(name != nullptr) {
        VkDebugUtilsObjectNameInfoEXT nameInfo = { VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT };
        nameInfo.objectType = VK_OBJECT_TYPE_BUFFER;
        nameInfo.objectHandle = (uint64_t)buffer;
        nameInfo.pObjectName = name;
        vkSetDebugUtilsObjectNameEXT(hwInterface->device, &nameInfo);
    }

    // Find it's memory requirement and allocate and bind memory as needed.
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(hwInterface->device, buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = VulkanFrontend::IdentifyMemoryType(memRequirements.memoryTypeBits, properties);
    vkAllocateMemory(hwInterface->device, &allocInfo, nullptr, &bufferMemory);
    vkBindBufferMemory(hwInterface->device, buffer, bufferMemory, 0);
}

void VulkanFrontend::CreateImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
    VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage &image, VkDeviceMemory &imageMemory,
    const char* name, VkImageCreateFlags flags)
{
    VkImageCreateInfo imageInfo = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO };
    imageInfo.flags = flags;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.format = format;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 1;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.tiling = tiling;
    imageInfo.usage = usage;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    vkCreateImage(hwInterface->device, &imageInfo, nullptr, &image);

    // Name the image.
    if(name != nullptr) {
        VkDebugUtilsObjectNameInfoEXT nameInfo = { VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT };
        nameInfo.objectType = VK_OBJECT_TYPE_IMAGE;
        nameInfo.objectHandle = (uint64_t)image;
        nameInfo.pObjectName = name;
        vkSetDebugUtilsObjectNameEXT(hwInterface->device, &nameInfo);
    }

    // Allocate memory to the Vulkan image.
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(hwInterface->device, image, &memRequirements);

    VkMemoryAllocateInfo allocInfo = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = VulkanFrontend::IdentifyMemoryType(memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    vkAllocateMemory(hwInterface->device, &allocInfo, nullptr, &imageMemory);
    vkBindImageMemory(hwInterface->device, image, imageMemory, 0);
}

VkImageView VulkanFrontend::CreateImageView(VkImage image, VkFormat format, VkImageViewCreateInfo *infoOut)
{
    VkImageViewCreateInfo imageViewInfo = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
    imageViewInfo.image = image;
    imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imageViewInfo.format = format;
    imageViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    imageViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    imageViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    imageViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageViewInfo.subresourceRange.baseMipLevel = 0;
    imageViewInfo.subresourceRange.levelCount = 1;
    imageViewInfo.subresourceRange.baseArrayLayer = 0;
    imageViewInfo.subresourceRange.layerCount = 1;

    if(infoOut != nullptr)
        memcpy(infoOut, &imageViewInfo, sizeof(VkImageViewCreateInfo));
    
    VkImageView imageView;
    vkCreateImageView(hwInterface->device, &imageViewInfo, nullptr, &imageView);
    return imageView;
}

VkShaderModule VulkanFrontend::CreateShaderModuleFromFile(const std::filesystem::path& filepath)
{
    // Open the SPIR-V bytecode file.
    std::ifstream file(filepath, std::ios::ate | std::ios::binary);
    if (!file.is_open()) {
        logCb(RETRO_LOG_ERROR, "Failed to open shader file %s! Does it exist?");
        return nullptr;
    }

    // Read the entire file into memory.
    size_t fileSize = (size_t) file.tellg();
    std::vector<char> codeBuffer(fileSize);
    file.seekg(0);
    file.read(codeBuffer.data(), fileSize);
    file.close();

    // Create the shader module.
    VkShaderModule module;
    VkShaderModuleCreateInfo moduleInfo = { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO };
    moduleInfo.codeSize = codeBuffer.size();
    moduleInfo.pCode = (const uint32_t*)codeBuffer.data();
    vkCreateShaderModule(hwInterface->device, &moduleInfo, nullptr, &module);

    return module;
}

void VulkanFrontend::OpenTemporaryCommand(VkCommandPool& commandPool, VkCommandBuffer& command)
{
    // Create transient command pool and single buffer.
    VkCommandPoolCreateInfo poolInfo = { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO };
    poolInfo.queueFamilyIndex = hwInterface->queue_index;
    poolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    vkCreateCommandPool(hwInterface->device, &poolInfo, nullptr, &commandPool);

    VkCommandBufferAllocateInfo bufferInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO };
    bufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    bufferInfo.commandBufferCount = 1;
    bufferInfo.commandPool = commandPool;
    vkAllocateCommandBuffers(hwInterface->device, &bufferInfo, &command);

    // Start recording the new command buffer.
    VkCommandBufferBeginInfo beginInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    vkBeginCommandBuffer(command, &beginInfo);
}

void VulkanFrontend::CloseAndSubmitCommand(VkCommandBuffer command, VkFence fence)
{
    // Close the command buffer.
    vkEndCommandBuffer(command);

    // If fence is present, make sure it is reset before submission.
    if(fence != nullptr) {
        vkResetFences(hwInterface->device, 1, &fence);
    }

    // Submit the command buffer.
    hwInterface->lock_queue(hwInterface->handle);
    VkSubmitInfo submitInfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &command;
    vkQueueSubmit(hwInterface->queue, 1, &submitInfo, fence);
    hwInterface->unlock_queue(hwInterface->handle);
}

void VulkanFrontend::RecordImageTransition(VkCommandBuffer command, VkImage image,
        VkImageLayout oldLayout, VkImageLayout newLayout)
{
    VkPipelineStageFlags srcStage, dstStage;

    VkImageMemoryBarrier barrier = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    // Determine accesses the barrier needs to wait for.
    switch(oldLayout) {
    case VK_IMAGE_LAYOUT_UNDEFINED:
        barrier.srcAccessMask = 0;
        srcStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        break;
    case VK_IMAGE_LAYOUT_PREINITIALIZED:
        barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
        srcStage = VK_PIPELINE_STAGE_HOST_BIT;
        break;
    case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
        barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        srcStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        break;
    case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
        barrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        srcStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        break;
    case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
        barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
        srcStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        break;
    case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        break;
    case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        break;
    default:
        srcStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        break;
    }

    // Determine accesses that will need to wait for this barrier.
    switch(newLayout) {
    case VK_IMAGE_LAYOUT_UNDEFINED:
        barrier.dstAccessMask = 0;
        dstStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        break;
    case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
        barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dstStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        break;
    case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
        barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        dstStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        break;
    case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        break;
    case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        break;
    case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        break;
    case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
        srcStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
        dstStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        break;
    default:
        dstStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        break;
    }

    vkCmdPipelineBarrier(command, srcStage, dstStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
}

void VulkanFrontend::RecordBufferImageCopy(VkCommandBuffer command, VkBuffer buffer, VkImage image,
    uint32_t width, uint32_t height)
{
    VkBufferImageCopy region = {};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    region.imageOffset = {0, 0, 0};
    region.imageExtent = {width, height, 1};

    vkCmdCopyBufferToImage(command, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
}

void VulkanFrontend::RecordImageCopy(VkCommandBuffer command, VkImage srcImage, VkImage dstImage,
    uint32_t width, uint32_t height)
{
    VkImageCopy region = {};
    region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.srcSubresource.baseArrayLayer = 0;
    region.srcSubresource.layerCount = 1;
    region.srcSubresource.mipLevel = 0;
    region.srcOffset = {0, 0, 0};
    region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.dstSubresource.baseArrayLayer = 0;
    region.dstSubresource.layerCount = 1;
    region.dstSubresource.mipLevel = 0;
    region.dstOffset = {0, 0, 0};
    region.extent = {width, height, 1};

    vkCmdCopyImage(command, srcImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dstImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
}

void VulkanFrontend::RecordRenderPassStart(VkCommandBuffer command, VkRenderPass renderPass,
        VkFramebuffer framebuffer, uint32_t width, uint32_t height,
        std::vector<VkClearValue> clearValues)
{
    VkRenderPassBeginInfo renderPassInfo = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO };
    renderPassInfo.renderPass = renderPass;
    renderPassInfo.framebuffer = framebuffer;
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = {width, height};
    renderPassInfo.clearValueCount = clearValues.size();
    renderPassInfo.pClearValues = clearValues.data();

    vkCmdBeginRenderPass(command, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
}