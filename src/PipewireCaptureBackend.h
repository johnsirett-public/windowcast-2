#pragma once

#include "CaptureBackend.h"

#include <memory>
#include <gio/gio.h>
#include <pipewire/pipewire.h>
#include <spa/param/video/format-utils.h>

typedef void (*XDPSignalRealCallback)(GVariant* parameters, void* userData);

class XDPSignalSubscription
{
private:
    GDBusConnection* dbusConnection;
    GCancellable* cancellable;
    XDPSignalRealCallback callback;
    std::string requestPath;
    gpointer userData;
    guint signalId;
    gulong cancelledId;

    static void OnCancelled(GCancellable* cancellable, void* data);
    static void OnResponseReceived(GDBusConnection* connection,
        const char* senderName, const char* objectPath, const char* interfaceName,
        const char* signalName, GVariant* parameters, void* userData);
public:
    XDPSignalSubscription(GDBusConnection* dbusConnection, const char* requestPath,
        GCancellable* cancellable, XDPSignalRealCallback callback, gpointer userData);
    ~XDPSignalSubscription();
};

/**
 * A capture backend based on PipeWire video streams and the XDG Desktop Portal, allowing capture of a single window or the whole screen.
 * Compatible with most Wayland compositors.
 */
class PipewireCaptureBackend : CaptureBackend
{
private:
    CaptureReadiness readiness;

    // XDP / D-Bus communication
    GCancellable* cancellable;
    GDBusConnection* dbusConnection;
    GDBusProxy* screencastProxy;
    std::string screencastSessionHandle;

    std::string GetObjectPathSafeSenderName();
    void CreateRequestPath(std::string* outPath, std::string* outToken);
    void CreateSessionPath(std::string* outPath, std::string* outToken);

    std::shared_ptr<XDPSignalSubscription> createSessionSignalSub;
    static void OnCreateSessionResponse(GVariant* parameters, void* userData);
    static void OnSessionCreated(GObject* source, GAsyncResult* inResult, void* userData);
    std::shared_ptr<XDPSignalSubscription> selectSourceSignalSub;
    static void OnSelectSourceResponse(GVariant* parameters, void* userData);
    static void OnSourceSelected(GObject* source, GAsyncResult* inResult, void* userData);
    std::shared_ptr<XDPSignalSubscription> startSignalSub;
    static void OnStartResponse(GVariant* parameters, void* userData);
    static void OnStarted(GObject* source, GAsyncResult* inResult, void* userData);
    static void OnPipewireRemoteOpened(GObject* source, GAsyncResult* inResult, void* userData);

    void XDPCreateSession();
    void XDPSelectSource();
    void XDPStart();
    void XDPOpenPipewireRemote();

    // PipeWire
    uint32_t pipewireNode;
    int pipewireFd;
    pw_thread_loop* pipewireThreadLoop;
    pw_context* pipewireContext;
    pw_core* pipewireCore;
    spa_hook pipewireCoreListener;
    int pipewireSyncId;

    pw_stream* pipewireStream;
    spa_hook pipewireStreamListener;
    spa_video_info pipewireStreamFormat;
    bool pipewireStreamNegotiated;

    bool PipewireConnectFd(int pipewireFd);
    void PipewireConnectStream();
    static void OnCoreInfo(void* userData, const pw_core_info *info);
    static void OnCoreDone(void* userData, uint32_t id, int seq);
    static void OnCoreError(void* userData, uint32_t id, int seq, int res, const char* message);
    static void OnParamChanged(void* userData, uint32_t id, const spa_pod* param);
    static void OnProcess(void* userData);

    // Vulkan and image processing
    VkFence updateFence;

    bool stagingBufferLock; // set to true if staging buffer should not be used right away
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    VkExtent2D imageExtent;
    VkImage image;
    VkDeviceMemory imageMemory;
    VkImageView imageView;

    void CleanupCaptureResources();
    void CreateNewVulkanStructures();
public:
    /**
     * Create a new PipeWire capture backend. No capture partials are required, because window selection is handled by an XDP portal system dialog.
     */
    PipewireCaptureBackend(bool captureCursor);
    ~PipewireCaptureBackend();
    
    void Update(void) override;
    bool SetupCapture(void) override;
    CaptureReadiness GetCaptureReadiness(void) override;
    void UpdateCapturedFrame(void) override;
    VkExtent2D GetCapturedFrameExtent(void) override;
    VkRect2D GetSuggestedCropRect(void) override;
    bool NeedsColorSwap(void) override;
    VkImage GetCapturedFrame(void) override;
    VkImageView GetCapturedFrameView(void) override;
};