#version 450

layout(location = 0) out vec2 fragTexCoord;

layout(push_constant) uniform pc {
    vec2 coordMult;
    vec2 coordShift;
    bool colorSwap;
};

vec2 positions[6] = vec2[](
    vec2(-1.0, -1.0), // Top left
    vec2( 1.0, -1.0), // Top right
    vec2(-1.0,  1.0), // Bottom left
    vec2(-1.0,  1.0), // Bottom left
    vec2( 1.0, -1.0), // Top right
    vec2( 1.0,  1.0)  // Bottom right
);

vec2 texCoords[6] = vec2[](
    vec2(0.0, 0.0), // Top left
    vec2(1.0, 0.0), // Top right
    vec2(0.0, 1.0), // Bottom left
    vec2(0.0, 1.0), // Bottom left
    vec2(1.0, 0.0), // Top right
    vec2(1.0, 1.0)  // Bottom right
);

void main() {
    gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);
    fragTexCoord = (texCoords[gl_VertexIndex] * coordMult) + coordShift;
}