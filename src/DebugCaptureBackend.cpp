#include "DebugCaptureBackend.h"

#include <cstring>
#include <memory>
#include "VulkanFrontend.h"

extern retro_environment_t                  environCb;
extern retro_log_printf_t                   logCb;
extern retro_hw_render_interface_vulkan*    hwInterface;

#define DEBUG_RENDER_WIDTH 720
#define DEBUG_RENDER_HEIGHT 480

void DebugCaptureBackend::CleanupCaptureResources()
{
    // Early bath if we aren't even set up for a capture yet.
    if(this->readiness != CaptureReadiness::READY)
        return;

    // Cleanup Vulkan resources.
    vkDeviceWaitIdle(hwInterface->device);
    vkDestroyImageView(hwInterface->device, this->imageView, nullptr);
    vkDestroyImage(hwInterface->device, this->image, nullptr);
    vkFreeMemory(hwInterface->device, this->imageMemory, nullptr);
}

DebugCaptureBackend::DebugCaptureBackend()
    : CaptureBackend({}, false), readiness(CaptureReadiness::DEAD)
{}

DebugCaptureBackend::~DebugCaptureBackend()
{
    CleanupCaptureResources();
}

bool DebugCaptureBackend::SetupCapture(void)
{
    // If we already have a capture ready, we need to cleanup old capture resources before we proceed.
    if(this->readiness == CaptureReadiness::READY)
        CleanupCaptureResources();

    // Create a staging buffer.
    VkDeviceSize imageSize = DEBUG_RENDER_WIDTH * DEBUG_RENDER_HEIGHT * 4;
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    VulkanFrontend::CreateBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer, stagingBufferMemory, "Debug Capture Backend Staging Buffer");

    // Software-rendered SMPTE EG 1-1990 SDTV color bars, rendered directly into staging buffer.
    uint8_t* pixels;
    vkMapMemory(hwInterface->device, stagingBufferMemory, 0, imageSize, 0, (void**)&pixels);

    uint twoThirdsHeight = (2*DEBUG_RENDER_HEIGHT)/3, eightPercentHeight = (2*DEBUG_RENDER_HEIGHT)/25;
    uint b = DEBUG_RENDER_WIDTH/7;
    uint y = 0;

    // 67% main color bars
    for (; y < twoThirdsHeight; y++) {
        uint x = 0;
        for (; x < b; x++) { // 75% white bar
            *pixels++ = 180; *pixels++ = 180; *pixels++ = 180; *pixels++ = 255;}
        for (; x < b*2; x++) { // 75% yellow bar
            *pixels++ = 180; *pixels++ = 180; *pixels++ = 16; *pixels++ = 255;}
        for (; x < b*3; x++) { // 75% cyan bar
            *pixels++ = 16; *pixels++ = 180; *pixels++ = 180; *pixels++ = 255;}
        for (; x < b*4; x++) { // 75% green bar
            *pixels++ = 16; *pixels++ = 180; *pixels++ = 16; *pixels++ = 255;}
        for (; x < b*5; x++) { // 75% magenta bar
            *pixels++ = 180; *pixels++ = 16; *pixels++ = 180; *pixels++ = 255;}
        for (; x < b*6; x++) { // 75% red bar
            *pixels++ = 180; *pixels++ = 16; *pixels++ = 16; *pixels++ = 255;}
        for (; x < DEBUG_RENDER_WIDTH; x++) { // 75% blue bar
            *pixels++ = 16; *pixels++ = 16; *pixels++ = 180; *pixels++ = 255;}
    }
    
    // Castellations
    for (; y < twoThirdsHeight+eightPercentHeight; y++) {
        uint x = 0;
        for (; x < b; x++) { // 75% blue
            *pixels++ = 16; *pixels++ = 16; *pixels++ = 180; *pixels++ = 255;}
        for (; x < b*2; x++) { // 75% black
            *pixels++ = 16; *pixels++ = 16; *pixels++ = 16; *pixels++ = 255;}
        for (; x < b*3; x++) { // 75% magenta
            *pixels++ = 180; *pixels++ = 16; *pixels++ = 180; *pixels++ = 255;}
        for (; x < b*4; x++) { // 75% black
            *pixels++ = 16; *pixels++ = 16; *pixels++ = 16; *pixels++ = 255;}
        for (; x < b*5; x++) { // 75% cyan
            *pixels++ = 16; *pixels++ = 180; *pixels++ = 180; *pixels++ = 255;}
        for (; x < b*6; x++) { // 75% black
            *pixels++ = 16; *pixels++ = 16; *pixels++ = 16; *pixels++ = 255;}
        for (; x < DEBUG_RENDER_WIDTH; x++) { // 75% white
            *pixels++ = 180; *pixels++ = 180; *pixels++ = 180; *pixels++ = 255;}
    }

    // Final color bars
    for (; y < DEBUG_RENDER_HEIGHT; y++) {
        uint fiveQuartersB = (5*b)/4, oneThirdB = b/3;
        uint x = 0;
        for (; x < fiveQuartersB; x++) { // -I
            *pixels++ = 16; *pixels++ = 70; *pixels++ = 106; *pixels++ = 255;}
        for (; x < fiveQuartersB*2; x++) { // 100% white
            *pixels++ = 235; *pixels++ = 235; *pixels++ = 235; *pixels++ = 255;}
        for (; x < fiveQuartersB*3; x++) { // +Q
            *pixels++ = 72; *pixels++ = 16; *pixels++ = 118; *pixels++ = 255;}
        for (; x < fiveQuartersB*4; x++) { // Reference black
            *pixels++ = 16; *pixels++ = 16; *pixels++ = 16; *pixels++ = 255;}
        for (; x < (fiveQuartersB*4)+oneThirdB; x++) { // PLUGE - super black
            *pixels++ = 6; *pixels++ = 6; *pixels++ = 6; *pixels++ = 255;}
        for (; x < (fiveQuartersB*4)+(oneThirdB*2); x++) { // PLUGE - reference black
            *pixels++ = 16; *pixels++ = 16; *pixels++ = 16; *pixels++ = 255;}
        for (; x < (fiveQuartersB*4)+(oneThirdB*3); x++) { // PLUGE - high black
            *pixels++ = 26; *pixels++ = 26; *pixels++ = 26; *pixels++ = 255;}
        for (; x < DEBUG_RENDER_WIDTH; x++) { // Reference black
            *pixels++ = 16; *pixels++ = 16; *pixels++ = 16; *pixels++ = 255;}
    }

    vkUnmapMemory(hwInterface->device, stagingBufferMemory);

    // Create the Vulkan image.
    VulkanFrontend::CreateImage(DEBUG_RENDER_WIDTH, DEBUG_RENDER_HEIGHT, VK_FORMAT_R8G8B8A8_SRGB,
        VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->image, this->imageMemory, "Debug Capture Backend Texture");

    // Create a view for the Vulkan image.
    this->imageView = VulkanFrontend::CreateImageView(this->image, VK_FORMAT_R8G8B8A8_SRGB);
    
    // Store the image extent.
    imageExtent.width = DEBUG_RENDER_WIDTH;
    imageExtent.height = DEBUG_RENDER_HEIGHT;

    // Transition the image's layout and load with contents.
    VkFence contentLoadFence;
    VkFenceCreateInfo fenceInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
    vkCreateFence(hwInterface->device, &fenceInfo, nullptr, &contentLoadFence);

    VkCommandPool localCommandPool;
    VkCommandBuffer localCmd;
    VulkanFrontend::OpenTemporaryCommand(localCommandPool, localCmd);

    VulkanFrontend::RecordImageTransition(localCmd, this->image,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    VulkanFrontend::RecordBufferImageCopy(localCmd, stagingBuffer, this->image,
        static_cast<uint32_t>(imageExtent.width), static_cast<uint32_t>(imageExtent.height));
    VulkanFrontend::RecordImageTransition(localCmd, this->image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    VulkanFrontend::CloseAndSubmitCommand(localCmd, contentLoadFence);

    vkWaitForFences(hwInterface->device, 1, &contentLoadFence, VK_TRUE, UINT64_MAX);

    // Clean up.
    vkDestroyFence(hwInterface->device, contentLoadFence, nullptr);

    vkFreeCommandBuffers(hwInterface->device, localCommandPool, 1, &localCmd);
    vkDestroyCommandPool(hwInterface->device, localCommandPool, nullptr);

    vkDestroyBuffer(hwInterface->device, stagingBuffer, nullptr);
    vkFreeMemory(hwInterface->device, stagingBufferMemory, nullptr);

    this->readiness = CaptureReadiness::READY;
    return true;
}

CaptureBackend::CaptureReadiness DebugCaptureBackend::GetCaptureReadiness(void)
{
    return this->readiness;
}

// Does nothing, since the frame is statically prepared in SetupCapture().
void DebugCaptureBackend::UpdateCapturedFrame(void)
{}

VkExtent2D DebugCaptureBackend::GetCapturedFrameExtent()
{
    return this->imageExtent;
}

VkRect2D DebugCaptureBackend::GetSuggestedCropRect()
{
    VkRect2D cropRect;
    cropRect.offset = {0, 0};
    cropRect.extent = {this->imageExtent.width, this->imageExtent.height};
    return cropRect;
}

bool DebugCaptureBackend::NeedsColorSwap(void)
{
    return false;
}

VkImage DebugCaptureBackend::GetCapturedFrame()
{
    return this->image;
}

VkImageView DebugCaptureBackend::GetCapturedFrameView()
{
    return this->imageView;
}