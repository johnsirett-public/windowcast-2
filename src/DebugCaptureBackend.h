#pragma once

#include "CaptureBackend.h"

#include <filesystem>

/**
 * A capture backend which displays a single texture loading from an image in the system directory.
 * Used for testing the frontend functionality of WindowCast.
 */
class DebugCaptureBackend : CaptureBackend
{
private:
    VkExtent2D imageExtent;
    VkImage image;
    VkDeviceMemory imageMemory;
    VkImageView imageView;

    CaptureReadiness readiness;

    void CleanupCaptureResources();
public:
    /**
     * Create a new color-bars debugging backend.
     */
    DebugCaptureBackend();
    ~DebugCaptureBackend();

    bool SetupCapture(void) override;
    CaptureReadiness GetCaptureReadiness(void) override;
    void UpdateCapturedFrame(void) override;
    VkExtent2D GetCapturedFrameExtent() override;
    VkRect2D GetSuggestedCropRect() override;
    bool NeedsColorSwap(void) override;
    VkImage GetCapturedFrame(void) override;
    VkImageView GetCapturedFrameView(void) override;
};