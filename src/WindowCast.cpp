#include <cstdio>
#include <cstdarg>
#include <cstring>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "vulkan/vulkan_symbol_wrapper.h"
#include "libretro_vulkan.h"

#include "VulkanFrontend.h"
#include "CaptureBackend.h"
#include "DebugCaptureBackend.h"

#ifdef __linux__
#include "XCompositeCaptureBackend.h"
#include "PipewireCaptureBackend.h"
#endif

using std::shared_ptr, std::make_shared;

// TODO: Test Debug Capture Backend on Windows, begin coding WGC backend for Windows based on WindowCast 1 source.

// ------------------------
// -- LIBRETRO INTERFACE --
// ------------------------

retro_environment_t         environCb;
retro_video_refresh_t       videoCb;
retro_audio_sample_t        audioCb;
retro_audio_sample_batch_t  audioBatchCb;
retro_input_poll_t          inputPollCb;
retro_input_state_t         inputStateCb;
retro_log_printf_t          logCb;

retro_hw_render_callback            hwRender;
retro_hw_render_interface_vulkan*   hwInterface;
retro_game_geometry                 savedGeometry;

unsigned frameCounter;

// -------------------
// -- STATE GLOBALS --
// -------------------

shared_ptr<VulkanFrontend> vkFrontend;
shared_ptr<CaptureBackend> captureBackend;

// ------------------------
// -- LIBRETRO INTERFACE --
// ------------------------

#define NOMINAL_WIDTH 256
#define NOMINAL_HEIGHT 256

static void fallbackLog(enum retro_log_level level, const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vfprintf(stderr, fmt, va);
    va_end(va);
}

// If these was a neater, cleaner way of specifying this other than just hardcoding these huge tables here, and it *acutally worked*, I'd do it, believe me.
#define RESO_VALUES \
{ "128", NULL }, { "144", NULL }, { "160", NULL }, { "176", NULL }, { "192", NULL }, { "208", NULL }, { "224", NULL }, { "240", NULL }, \
{ "256", NULL }, { "272", NULL }, { "288", NULL }, { "304", NULL }, { "320", NULL }, { "336", NULL }, { "352", NULL }, { "368", NULL }, \
{ "384", NULL }, { "400", NULL }, { "416", NULL }, { "432", NULL }, { "448", NULL }, { "464", NULL }, { "480", NULL }, { "496", NULL }, \
{ "512", NULL }, { "528", NULL }, { "544", NULL }, { "560", NULL }, { "576", NULL }, { "592", NULL }, { "600", NULL }, { "608", NULL }, \
{ "624", NULL }, { "640", NULL }, { "656", NULL }, { "672", NULL }, { "688", NULL }, { "704", NULL }, { "720", NULL }, { "736", NULL }, \
{ "752", NULL }, { "768", NULL }, { "784", NULL }, { "800", NULL }, { "816", NULL }, { "832", NULL }, { "848", NULL }, { "864", NULL }, \
{ "880", NULL }, { "896", NULL }, { "900", NULL }, { "912", NULL }, { "928", NULL }, { "944", NULL }, { "960", NULL }, { "976", NULL }, \
{ "992", NULL }, { "1008", NULL }, { "1024", NULL }, { "1040", NULL }, { "1056", NULL }, { "1072", NULL }, { "1080", NULL }, { "1088", NULL }, \
{ "1104", NULL }, { "1120", NULL }, { "1136", NULL }, { "1152", NULL }, { "1168", NULL }, { "1184", NULL }, { "1200", NULL }, { "1216", NULL }, \
{ "1232", NULL }, { "1248", NULL }, { "1264", NULL }, { "1280", NULL }, { "1296", NULL }, { "1312", NULL }, { "1328", NULL }, { "1344", NULL }, \
{ "1360", NULL }, { "1376", NULL }, { "1392", NULL }, { "1408", NULL }, { "1424", NULL }, { "1440", NULL }, { "1456", NULL }, { "1472", NULL }, \
{ "1488", NULL }, { "1504", NULL }, { "1520", NULL }, { "1536", NULL }, { "1552", NULL }, { "1568", NULL }, { "1584", NULL }, { "1600", NULL }, \
{ "1616", NULL }, { "1632", NULL }, { "1648", NULL }, { "1664", NULL }, { "1680", NULL }, { "1696", NULL }, { "1712", NULL }, { "1728", NULL }, \
{ "1744", NULL }, { "1760", NULL }, { "1776", NULL }, { "1792", NULL }, { "1808", NULL }, { "1824", NULL }, { "1840", NULL }, { "1856", NULL }, \
{ "1872", NULL }, { "1888", NULL }, { "1904", NULL }, { "1920", NULL }, { "2160", NULL }, { "3840", NULL }, { "4096", NULL }, { "4320", NULL }, \
{ "7680", NULL }, { "8192", NULL }, { "16384", NULL }, { "source", "Don't scale this axis." }, { NULL, NULL }
#define MULT_VALUES \
{ "1", "1x" }, { "2", "2x" }, { "3", "3x" }, { "4", "4x" }, { "5", "5x" }, { "6", "6x" }, \
{ "0.0625", "0.0625x (1/16)"},  { "0.125", "0.125x (1/8)"}, { "0.25", "0.25x (1/4)" }, { "0.334", "0.334x (1/3)"}, \
{ "0.5", "0.5x (1/2)" }, { "0.667", "0.667x (2/3)" }, { "0.75", "0.75x (3/4)" }, { NULL, NULL }
#define ASPECT_VALUES \
{ "1.0", "Force 1:1" }, { "1.25", "Force 5:4" }, { "1.333", "Force 4:3" }, { "1.5", "Force 3:2" }, \
{ "1.6", "Force 16:10" }, { "1.778", "Force 16:9" }, { "1.85", "Force 1.85:1" }, { "2.333", "Force true 21:9" }, \
{ "2.39", "Force 2.39:1 ('21:9')" }, {"3.556", "Force 32:9"}, {"4.0", "Force 4:1; Vive la revolution!"}, { NULL, NULL }
#define FINE_VALUES \
{ "0", NULL }, { "1", NULL }, { "2", NULL }, { "3", NULL }, { "4", NULL }, { "5", NULL }, { "6", NULL }, { "7", NULL }, \
{ "8", NULL }, { "9", NULL }, { "10", NULL }, { "11", NULL }, { "12", NULL }, { "13", NULL }, { "14", NULL }, { "15", NULL }, \
{ "16", NULL }, { "17", NULL }, { "18", NULL }, { "19", NULL }, { "20", NULL }, { "21", NULL }, { "22", NULL }, { "23", NULL }, \
{ "24", NULL }, { "25", NULL }, { "26", NULL }, { "27", NULL }, { "28", NULL }, { "29", NULL }, { "30", NULL }, { "31", NULL }, \
{ "32", NULL }, { "33", NULL }, { "34", NULL }, { "35", NULL }, { "36", NULL }, { "37", NULL }, { "38", NULL }, { "39", NULL }, \
{ "40", NULL }, { "41", NULL }, { "42", NULL }, { "43", NULL }, { "44", NULL }, { "45", NULL }, { "46", NULL }, { "47", NULL }, \
{ "48", NULL }, { "49", NULL }, { "50", NULL }, { "51", NULL }, { "52", NULL }, { "53", NULL }, { "54", NULL }, { "55", NULL }, \
{ "56", NULL }, { "57", NULL }, { "58", NULL }, { "59", NULL }, { "60", NULL }, { "61", NULL }, { "62", NULL }, { "63", NULL }, \
{ "64", NULL }, { "65", NULL }, { "66", NULL }, { "67", NULL }, { "68", NULL }, { "69", NULL }, { "70", NULL }, { "71", NULL }, \
{ "72", NULL }, { "73", NULL }, { "74", NULL }, { "75", NULL }, { "76", NULL }, { "77", NULL }, { "78", NULL }, { "79", NULL }, \
{ "80", NULL }, { "81", NULL }, { "82", NULL }, { "83", NULL }, { "84", NULL }, { "85", NULL }, { "86", NULL }, { "87", NULL }, \
{ "88", NULL }, { "89", NULL }, { "90", NULL }, { "91", NULL }, { "92", NULL }, { "93", NULL }, { "94", NULL }, { "95", NULL }, \
{ "96", NULL }, { "97", NULL }, { "98", NULL }, { "99", NULL }
#define COARSE_VALUES \
{ "0", NULL }, { "100", NULL }, { "200", NULL }, { "300", NULL }, { "400", NULL }, { "500", NULL }, { "600", NULL }, { "700", NULL }, \
{ "800", NULL }, { "900", NULL }, { "1000", NULL }, { "1100", NULL }, { "1200", NULL }, { "1300", NULL }, { "1400", NULL }, { "1500", NULL }, \
{ "1600", NULL }, { "1700", NULL }, { "1800", NULL }, { "1900", NULL }, { "2000", NULL }, { "2100", NULL }, { "2200", NULL }, { "2300", NULL }, \
{ "2400", NULL }, { "2500", NULL }, { "2600", NULL }, { "2700", NULL }, { "2800", NULL }, { "2900", NULL }, { "3000", NULL }, { "3100", NULL }, \
{ "3200", NULL }, { "3300", NULL }, { "3400", NULL }, { "3500", NULL }, { "3600", NULL }, { "3700", NULL }, { "3800", NULL }, { "3900", NULL }, \
{ "4000", NULL }, { "4100", NULL }, { "4200", NULL }, { "4300", NULL }, { "4400", NULL }, { "4500", NULL }, { "4600", NULL }, { "4700", NULL }, \
{ "4800", NULL }, { "4900", NULL }, { "5000", NULL }, { "5100", NULL }, { "5200", NULL }, { "5300", NULL }, { "5400", NULL }, { "5500", NULL }, \
{ "5600", NULL }, { "5700", NULL }, { "5800", NULL }, { "5900", NULL }, { "6000", NULL }, { "6100", NULL }, { "6200", NULL }, { "6300", NULL }, \
{ "6400", NULL }, { "6500", NULL }, { "6600", NULL }, { "6700", NULL }, { "6800", NULL }, { "6900", NULL }, { "7000", NULL }, { "7100", NULL }, \
{ "7200", NULL }, { "7300", NULL }, { "7400", NULL }, { "7500", NULL }, { "7600", NULL }, { "7700", NULL }, { "7800", NULL }, { "7900", NULL }, \
{ "8000", NULL }, { "8100", NULL }, { "8200", NULL }, { "8300", NULL }, { "8400", NULL }, { "8500", NULL }, { "8600", NULL }, { "8700", NULL }, \
{ "8800", NULL }, { "8900", NULL }, { "9000", NULL }, { "9100", NULL }, { "9200", NULL }, { "9300", NULL }, { "9400", NULL }, { "9500", NULL }, \
{ "9600", NULL }, { "9700", NULL }, { "9800", NULL }, { "9900", NULL }

#define RETRO_VARIABLE(x) \
retro_variable x = {}; (x).key = #x; \
environCb(RETRO_ENVIRONMENT_GET_VARIABLE, &x); \
if ((x).value == NULL) { logCb(RETRO_LOG_WARN, "Can't get value of variable %s, so nulling. Expect the unexpected!\n", #x); (x).value = ""; }

void retro_set_environment(retro_environment_t cb)
{
    // Store environment callback.
    environCb = cb;

    // Setup logging callback.
    static retro_log_callback logInterface;
    if (cb(RETRO_ENVIRONMENT_GET_LOG_INTERFACE, &logInterface))
        logCb = logInterface.log;
    else
        logCb = fallbackLog;
    
    // Setup core options.
    static const retro_core_option_definition coreOptions[] = {
        // Capture options.
        { "capture_cursor", "Show cursor on capture",
            "Whether to show the mouse cursor as part of the capture. Requires reset if changed.",
            {{"disabled", "Don't show cursor"}, {"enabled", "Show cursor"},
             { NULL, NULL }}, "disabled"},
        // Scaling options.
        { "scale_enable", "Enable downscaling/upscaling",
            "Enables downscaling/upscaling captured frames to a different internal resolution.",
            {{"disabled", "Disable scaling"}, {"exact", "Scale to an exact resolution (specified below)"},
             {"multiple", "Scale to a multiple of the captured window's resolution (specified below)"},
             { NULL, NULL }}, "disabled"},
        { "scale_exact_w", "Exact width for scaling",
            "Scale captured frames to this width if exact scaling is enabled above.",
            { RESO_VALUES }, "1280" },
        { "scale_exact_h", "Exact height for scaling",
            "Scale captured frames to this height if exact scaling is enabled above.",
            { RESO_VALUES }, "720" },
        { "scale_mult_w", "Width multiple for scaling",
            "Scale captured frame width by this multiplier if multiple scaling is enabled above.",
            { MULT_VALUES }, "2"},
        { "scale_mult_h", "Height multiple for scaling",
            "Scale captured frame height by this multiplier if multiple scaling is enabled above.",
            { MULT_VALUES }, "2" },
        { "scale_filter", "Filtering mode for scaling",
            "Changes texture filter used when downscaling/upscaling",
            {{"linear", "Scale using linear filtering"},
            {"point", "Scale using neighest-neighbor (no filtering)"},
            { NULL, NULL }}, "linear"},
        // Aspect ratio options.
        { "aspect_ratio", "Aspect ratio mode",
            "Choose what method is used to report core aspect ratio to libretro."
            " This value is used if \"Core Provided\" is set in RA's Settings->Video->Scaling.",
            {{"captured", "Use the aspect ratio of the captured & cropped frame, pre-scaling (default)"},
             {"scaled", "Use the aspect ratio of the scaled frame"}, ASPECT_VALUES }, "captured" },
        // Cropping options.
        { "crop_bypass", "Bypass backend cropping",
            "Automatic cropping according to the backend is typically used to crop titlebars and window-frames."
            " If this option is enabled, this will be bypassed, and WindowCast will display the entire frame."
            " Automatic cropping is applied before the custom crop below.",
            {{"false", "Not bypassed"}, {"true", "Bypassed"}, { NULL, NULL }}, "false" },
        { "custom_crop_left_fine", "Custom crop: left (fine-adjust)",
                "Crop the left of the captured frame by this many pixels.", { FINE_VALUES }, "0"},
        { "custom_crop_right_fine", "Custom crop: right (fine-adjust)",
                "Crop the right of the captured frame by this many pixels.", { FINE_VALUES }, "0"},
        { "custom_crop_top_fine", "Custom crop: top (fine-adjust)",
                "Crop the top of the captured frame by this many pixels.", { FINE_VALUES }, "0"},
        { "custom_crop_bottom_fine", "Custom crop: bottom (fine-adjust)",
                "Crop the bottom of the captured frame by this many pixels.", { FINE_VALUES }, "0"},
        { "custom_crop_left_coarse", "Custom crop: left (coarse-adjust)",
                "Crop the left of the captured frame by this many pixels.", { COARSE_VALUES }, "0" },
        { "custom_crop_right_coarse", "Custom crop: right (coarse-adjust)",
                "Crop the right of the captured frame by this many pixels.", { COARSE_VALUES }, "0"},
        { "custom_crop_top_coarse", "Custom crop: top (coarse-adjust)",
                "Crop the top of the captured frame by this many pixels.", { COARSE_VALUES }, "0"},
        { "custom_crop_bottom_coarse", "Custom crop: bottom (coarse-adjust)",
                "Crop the bottom of the captured frame by this many pixels.", { COARSE_VALUES }, "0"},
        { NULL, NULL, NULL, {{0}}, NULL }
    };
    cb(RETRO_ENVIRONMENT_SET_CORE_OPTIONS, (void*) &coreOptions);

    bool noRom = false;
    cb(RETRO_ENVIRONMENT_SET_SUPPORT_NO_GAME, &noRom);
}

void retro_set_video_refresh(retro_video_refresh_t cb)
{
    videoCb = cb;
}

void retro_set_audio_sample(retro_audio_sample_t cb)
{
    audioCb = cb;
}

void retro_set_audio_sample_batch(retro_audio_sample_batch_t cb)
{
    audioBatchCb = cb;
}

void retro_set_input_poll(retro_input_poll_t cb)
{
    inputPollCb = cb;
}

void retro_set_input_state(retro_input_state_t cb)
{
    inputStateCb = cb;
}

void retro_init(void)
{}

void retro_deinit(void)
{}

unsigned retro_api_version(void)
{
    return RETRO_API_VERSION;
}

void retro_get_system_info(struct retro_system_info *info)
{
    *info = {};
    info->library_name      = "WindowCast";
    info->library_version   = "2.0";
    info->valid_extensions  = "txt";
    info->need_fullpath     = false;
}

void retro_get_system_av_info(struct retro_system_av_info *info)
{
    info->geometry = {
        .base_width     = NOMINAL_WIDTH,
        .base_height    = NOMINAL_HEIGHT,
        .max_width      = NOMINAL_WIDTH,
        .max_height     = NOMINAL_HEIGHT,
        .aspect_ratio   = 0.0
    };
    info->timing = {
        .fps            = 60.0,
        .sample_rate    = 0.0
    };
}

void retro_set_controller_port_device(unsigned port, unsigned device)
{}

void retro_reset(void)
{
    // Setup the capture backend.
    if(!captureBackend->SetupCapture()) {
        logCb(RETRO_LOG_ERROR, "For whatever reason, WindowCast couldn't set up the capture backend. Closing the core now.\n");
        environCb(RETRO_ENVIRONMENT_SHUTDOWN, NULL);
        return;
    }

    // Reset the master frame counter.
    frameCounter = 0;
}

void retro_run()
{
    // Poll input (per spec).
    inputPollCb();

    // Refresh Vulkan H/W rendering interface. Sometimes we lose it, and that causes no end of issues.
    // TODO: I really don't like having to do this, it feels heavy, but it seems to be the only way to prevent the windowed mode / FS changeover crash.
    if(!environCb(RETRO_ENVIRONMENT_GET_HW_RENDER_INTERFACE, (void**)&hwInterface) || !hwInterface)
    {
        logCb(RETRO_LOG_ERROR, "Failed to refresh Vulkan HW rendering interface!\n");
        return;
    }

    // Check if the Vulkan frontend needs to be recreated.
    if (hwInterface->get_sync_index_mask(hwInterface->handle) != vkFrontend->GetSwapchainMask())
    {
        vkFrontend.reset();
        vkFrontend = make_shared<VulkanFrontend>();
    }

    // Unconditional update for backend.
    captureBackend->Update();

    // Check for captured frame and submit to the Vulkan frontend if we have it.
    CaptureBackend::CaptureReadiness captureReadiness = captureBackend->GetCaptureReadiness();
    if(captureReadiness == CaptureBackend::CaptureReadiness::READY) {
        // Update to the latest capture.
        captureBackend->UpdateCapturedFrame();

        // Now source the captured frame and it's info.
        VkExtent2D captureExtent = captureBackend->GetCapturedFrameExtent();
        VkImageView capturedFrameView = captureBackend->GetCapturedFrameView();

        // Get the cropping rectangle from the backend, or bypass if requested.
        VkRect2D cropRect;
        RETRO_VARIABLE(crop_bypass)
        if(!strcmp(crop_bypass.value, "true")) {
            cropRect.offset = {0, 0};
            cropRect.extent = captureExtent;
        } else
            cropRect = captureBackend->GetSuggestedCropRect();

        // Adjust the cropping if needed -- custom crop.
        RETRO_VARIABLE(custom_crop_left_fine);
        RETRO_VARIABLE(custom_crop_right_fine);
        RETRO_VARIABLE(custom_crop_top_fine);
        RETRO_VARIABLE(custom_crop_bottom_fine);
        RETRO_VARIABLE(custom_crop_left_coarse);
        RETRO_VARIABLE(custom_crop_right_coarse);
        RETRO_VARIABLE(custom_crop_top_coarse);
        RETRO_VARIABLE(custom_crop_bottom_coarse);
        uint32_t cropLeft   = atoi(custom_crop_left_fine.value) + atoi(custom_crop_left_coarse.value);
        uint32_t cropRight  = atoi(custom_crop_right_fine.value) + atoi(custom_crop_right_coarse.value);
        uint32_t cropTop    = atoi(custom_crop_top_fine.value) + atoi(custom_crop_top_coarse.value);
        uint32_t cropBottom = atoi(custom_crop_bottom_fine.value) + atoi(custom_crop_bottom_coarse.value);
        cropRect.offset.x += cropLeft;
        cropRect.offset.y += cropTop;
        cropRect.extent.width -= (cropLeft+cropRight);
        cropRect.extent.height -= (cropTop+cropBottom);

        // Figure out the push constants for the shader.
        VulkanFrontend::PushConstants pushConstants;
        pushConstants.coordMult = glm::vec2(
            (float)cropRect.extent.width / (float)captureExtent.width,
            (float)cropRect.extent.height / (float)captureExtent.height);
        pushConstants.coordShift = glm::vec2(
            (float)cropRect.offset.x / (float)captureExtent.width,
            (float)cropRect.offset.y / (float)captureExtent.height);
        pushConstants.colorSwap = captureBackend->NeedsColorSwap();

        // Now is the time to handle scaling.
        // Start by assuming the default "scaling disabled" conidition.
        // We use the crop extent as a basis for scaled extent, not the given capture extent.
        uint16_t scaledWidth = cropRect.extent.width;
        uint16_t scaledHeight = cropRect.extent.height;
        RETRO_VARIABLE(scale_enable);
        if(!strcmp(scale_enable.value, "exact")) {
            RETRO_VARIABLE(scale_exact_w);
            RETRO_VARIABLE(scale_exact_h);
            if(strcmp(scale_exact_w.value, "source"))
                scaledWidth = atoi(scale_exact_w.value);
            if(strcmp(scale_exact_h.value, "source"))
                scaledHeight = atoi(scale_exact_h.value);
        } else if (!strcmp(scale_enable.value, "multiple")) {
            RETRO_VARIABLE(scale_mult_w);
            RETRO_VARIABLE(scale_mult_h);
            scaledWidth = (float)cropRect.extent.width * atof(scale_mult_w.value);
            scaledHeight = (float)cropRect.extent.height * atof(scale_mult_h.value);
        }

        // Report scaling to Vulkan frontend.
        vkFrontend->RequestSwapExtent(scaledWidth, scaledHeight);

        // Calculate new aspect ratio / geometry and inform libretro about changes to such if needed.
        retro_game_geometry newGeometry;
        newGeometry.base_width  = scaledWidth;
        newGeometry.base_height = scaledHeight;

        RETRO_VARIABLE(aspect_ratio);
        if (!strcmp(aspect_ratio.value, "scaled"))
            newGeometry.aspect_ratio = (float)scaledWidth / (float)scaledHeight;
        else if (!strcmp(aspect_ratio.value, "captured"))
            newGeometry.aspect_ratio = (float)cropRect.extent.width / (float)cropRect.extent.height;
        else // Otherwise, we're forcing a specific aspect ratio value.
            newGeometry.aspect_ratio = atof(aspect_ratio.value);

        if(savedGeometry.base_width != newGeometry.base_width ||
           savedGeometry.base_height != newGeometry.base_height ||
           savedGeometry.aspect_ratio != newGeometry.aspect_ratio) {
            environCb(RETRO_ENVIRONMENT_SET_GEOMETRY, &newGeometry);
            savedGeometry = newGeometry;
        }

        // Work out what kind scaling/sampling was requested.
        VulkanFrontend::SamplingType samplingType = VulkanFrontend::SamplingType::LINEAR;
        RETRO_VARIABLE(scale_filter);
        if(!strcmp(scale_filter.value, "point"))
            samplingType = VulkanFrontend::SamplingType::POINT;
        
        // Now trigger the Vulkan frontend to blit to Libretro.
        vkFrontend->BlitImage(capturedFrameView, samplingType, pushConstants);
    } else if (captureReadiness == CaptureBackend::CaptureReadiness::DEAD) {
        retro_reset();
    }

    // Increment the master frame counter.
    frameCounter++;
}

size_t retro_serialize_size(void)
{
    return 0;
}

bool retro_serialize(void *data, size_t size)
{
    return false;
}

bool retro_unserialize(const void *data, size_t size)
{
    return false;
}

void retro_cheat_reset(void)
{}

void retro_cheat_set(unsigned index, bool enabled, const char *code)
{}

static void hwContextReset(void)
{
    // Get Vulkan H/W rendering interface.
    if(!environCb(RETRO_ENVIRONMENT_GET_HW_RENDER_INTERFACE, (void**)&hwInterface) || !hwInterface)
    {
        logCb(RETRO_LOG_ERROR, "Failed to get Vulkan HW rendering interface!\n");
        return;
    }

    // Check if HW render interface version is correct.
    if(hwInterface->interface_version != RETRO_HW_RENDER_INTERFACE_VULKAN_VERSION)
    {
        logCb(RETRO_LOG_ERROR, "HW render interface version mismatch, expected %u, got %u!\n",
            RETRO_HW_RENDER_INTERFACE_VULKAN_VERSION, hwInterface->interface_version);
        hwInterface = nullptr;
        return;
    }

    // Reload Vulkan symbols now we have a stable device.
    vulkan_symbol_wrapper_init(hwInterface->get_instance_proc_addr);
    vulkan_symbol_wrapper_load_core_instance_symbols(hwInterface->instance);
    vulkan_symbol_wrapper_load_core_device_symbols(hwInterface->device);

    // Initialize the Vulkan frontend state (RAII).
    vkFrontend = make_shared<VulkanFrontend>();
}

static void hwContextDestroy()
{
    // Need to destroy capture backend before Vulkan frontend, in case the capture backend used Vulkan structures.
    captureBackend.reset();
    vkFrontend.reset();
    hwInterface = nullptr;
}

bool retro_load_game(const struct retro_game_info *info)
{
    // Load capture partials from provided text file.
    std::vector<std::string> rawCapturePartials;
    const char* whitespaces = " \n\t\r\f\v";
    if(info) {
        if(info->size > 0) {
            std::stringstream ss((char*)info->data);
            std::string line;
            while (std::getline(ss, line, '\n')) {
                if(line.find_first_of('#') != std::string::npos)
                    line.erase(line.find_first_of('#'));            // Comment support.
                line.erase(0, line.find_first_not_of(whitespaces)); // Strip leading whitespace.
                line.erase(line.find_last_not_of(whitespaces) + 1); // Strip trailing whitespace.
                if (line.empty())
                    continue;

                rawCapturePartials.push_back(line);
            }
        } else {
            logCb(RETRO_LOG_WARN, "No content in loaded capture partials data. This will occur if you load an empty .txt file with WindowCast.\n");
        }
    } else {
        logCb(RETRO_LOG_WARN, "WindowCast was somehow loaded without content. Maybe the metadata wasn't properly installed?\n");
    }

    // Warn if we have no capture partials.
    if (rawCapturePartials.size() == 0) {
        logCb(RETRO_LOG_WARN, "The list of capture partials is empty. WindowCast will continue, but heck if I know what window it will capture.\n");
    }

    // Split out priorites from the capture partials.
    std::vector<CapturePartial_t> capturePartials;
    for (auto rawPartial : rawCapturePartials) {
        double priority = 1.0;
        std::string capturePartial;
        if(rawPartial.find_last_of("[") != std::string::npos) {
            std::string priorityString = rawPartial.substr(rawPartial.find_last_of("[") + 1);
            priority = atof(priorityString.c_str());
            capturePartial = rawPartial.substr(0, rawPartial.find_last_of("["));
            capturePartial.erase(rawPartial.find_last_not_of(whitespaces) + 1); // Strip trailing whitespace.
        } else {
            capturePartial = rawPartial;
        }

        logCb(RETRO_LOG_INFO, "Capture partial read: %s, with priority %f.\n",
            capturePartial.c_str(), priority);

        capturePartials.push_back(CapturePartial_t(capturePartial, priority));
    }    

    // Set up the H/W context to render via Vulkan.
    hwRender.context_type       = RETRO_HW_CONTEXT_VULKAN;
    hwRender.version_major      = VULKAN_API_VERSION;
    hwRender.version_minor      = 0;
    hwRender.context_reset      = hwContextReset;
    hwRender.context_destroy    = hwContextDestroy;
    hwRender.cache_context      = true;
    if (!environCb(RETRO_ENVIRONMENT_SET_HW_RENDER, &hwRender))
        return false;

    static retro_hw_render_context_negotiation_interface_vulkan iface;
    iface.interface_type        = RETRO_HW_RENDER_CONTEXT_NEGOTIATION_INTERFACE_VULKAN;
    iface.interface_version     = RETRO_HW_RENDER_CONTEXT_NEGOTIATION_INTERFACE_VULKAN_VERSION;
    iface.get_application_info  = VulkanFrontend::GetApplicationInfo;
    iface.create_instance       = VulkanFrontend::CreateInstance;
    // Pontential bug in RA 1.15.0, it won't call CD2 if we don't have a non-zero CD address.
    iface.create_device         = (retro_vulkan_create_device_t)0x42069;
    iface.create_device2        = VulkanFrontend::CreateDevice;
    environCb(RETRO_ENVIRONMENT_SET_HW_RENDER_CONTEXT_NEGOTIATION_INTERFACE, (void*)&iface);

    // Create the capture backend.
    // TODO: Determine the correct backend to create via an option.
    RETRO_VARIABLE(capture_cursor);
    bool captureCursor = strcmp(capture_cursor.value, "enabled") == 0;
    captureBackend = shared_ptr<CaptureBackend>((CaptureBackend*)new PipewireCaptureBackend(captureCursor));

    return true;
}

bool retro_load_game_special(unsigned game_type, const struct retro_game_info *info, size_t num_info)
{
    return false;
}

void retro_unload_game(void)
{}

unsigned retro_get_region(void)
{
    return RETRO_REGION_NTSC;
}

void *retro_get_memory_data(unsigned id)
{
    return NULL;
}

size_t retro_get_memory_size(unsigned id)
{
    return 0;
}