# Core design requirements for WindowCast 2.0

- Well-isolated capture backend layer and libretro layer.
- Pure Vulkan upload, with all postproc handled in Vulkan layer. Deprecate software upload layer, produce a Vulkan-only core.
- Capture backends:
    - DEBUGGING: Software SMPTE ECR 1-1978 style color bar generator.
    - WGC for Windows 10, version 1803 and later and Windows 11.
    - XComposite for Linux X11 desktop.
    - Pipewire for Linux Wayland desktop/Gamescope.
    - Others (not a priority to implement):
        - BitBlt for downlevel Windows / legacy capture on Windows.
        - Something for macOS support.
- Processing features (see options for WindowCast 1.0):
    - Upscale/downscale:
        - Exact resolution and multiple resolution modes.
        - Point, linear, and ideally, Lanczos filtering.
    - Aspect ratio reporting modes: cropped frame, scaled frame, or forced.
    - Arbitary crop.
